package com.playdeezgames.Avagadro;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import com.playdeezgames.common.Controller.AbstractControllerBase;
import com.playdeezgames.common.Controller.AbstractControllerListener;
import com.playdeezgames.common.Controller.ControllerAxis;
import com.playdeezgames.common.Controller.ControllerButton;
import com.playdeezgames.common.Controller.ControllerButtonState;
import com.playdeezgames.common.Controller.ControllerDPadState;

public class XBoxAbstractController extends AbstractControllerBase implements ControllerListener{

	public XBoxAbstractController(int theId,Controller theController) {
		super(theId);
		theController.addListener(this);
	}

	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		return false;
	}

	@Override
	public boolean axisMoved(Controller arg0, int arg1, float arg2) {
		ControllerAxis theAxis=null;
		float theAxisValue = arg2;
		switch(arg1){
		case 0:
			theAxis = ControllerAxis.LEFT_STICK_Y;
			break;
		case 1:
			theAxis = ControllerAxis.LEFT_STICK_X;
			break;
		case 2:
			theAxis = ControllerAxis.RIGHT_STICK_Y;
			break;
		case 3:
			theAxis = ControllerAxis.RIGHT_STICK_X;
			break;
		case 4:
			if(arg2<0){
				theAxisValue=-arg2;
				theAxis = ControllerAxis.RIGHT_TRIGGER;
			}else{
				theAxis = ControllerAxis.LEFT_TRIGGER;
			}
			break;
		default:
			return false;
		}
		for(AbstractControllerListener listener:listeners){
			listener.onAxisChange(getId(), theAxis, theAxisValue);
		}
		return true;
	}

	@Override
	public boolean buttonDown(Controller arg0, int arg1) {
		ControllerButton theButton;
		switch(arg1){
		case 0:
			theButton = ControllerButton.GREEN;
			break;
		case 1:
			theButton = ControllerButton.RED;
			break;
		case 2:
			theButton = ControllerButton.BLUE;
			break;
		case 3:
			theButton = ControllerButton.YELLOW;
			break;
		case 4:
			theButton = ControllerButton.LEFT_SHOULDER;
			break;
		case 5:
			theButton = ControllerButton.RIGHT_SHOULDER;
			break;
		case 7:
			theButton = ControllerButton.PAUSE;
			break;
		case 8:
			theButton = ControllerButton.LEFT_STICK;
			break;
		case 9:
			theButton = ControllerButton.RIGHT_STICK;
			break;
		default:
			return false;
		}
		for(AbstractControllerListener listener:listeners){
			listener.onButtonChange(getId(), theButton, ControllerButtonState.PRESSED);
		}
		return true;
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		ControllerButton theButton;
		switch(arg1){
		case 0:
			theButton = ControllerButton.GREEN;
			break;
		case 1:
			theButton = ControllerButton.RED;
			break;
		case 2:
			theButton = ControllerButton.BLUE;
			break;
		case 3:
			theButton = ControllerButton.YELLOW;
			break;
		case 4:
			theButton = ControllerButton.LEFT_SHOULDER;
			break;
		case 5:
			theButton = ControllerButton.RIGHT_SHOULDER;
			break;
		case 7:
			theButton = ControllerButton.PAUSE;
			break;
		case 8:
			theButton = ControllerButton.LEFT_STICK;
			break;
		case 9:
			theButton = ControllerButton.RIGHT_STICK;
			break;
		default:
			return false;
		}
		for(AbstractControllerListener listener:listeners){
			listener.onButtonChange(getId(), theButton, ControllerButtonState.RELEASED);
		}
		return true;
	}

	@Override
	public void connected(Controller arg0) {
	}

	@Override
	public void disconnected(Controller arg0) {
	}

	@Override
	public boolean povMoved(Controller arg0, int arg1, PovDirection arg2) {
		ControllerDPadState state;
		switch(arg2){
		case north:
			state = ControllerDPadState.NORTH;
			break;
		case south:
			state = ControllerDPadState.SOUTH;
			break;
		case east:
			state = ControllerDPadState.EAST;
			break;
		case west:
			state = ControllerDPadState.WEST;
			break;
		case northEast:
			state = ControllerDPadState.NORTHEAST;
			break;
		case southEast:
			state = ControllerDPadState.SOUTHEAST;
			break;
		case northWest:
			state = ControllerDPadState.NORTHWEST;
			break;
		case southWest:
			state = ControllerDPadState.SOUTHWEST;
			break;
		default:
			state = ControllerDPadState.CENTER;
			break;
		}
		for(AbstractControllerListener listener:listeners){
			listener.onDPadChange(getId(), state);
		}
		return true;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}


}
