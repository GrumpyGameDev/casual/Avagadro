package com.playdeezgames.Avagadro;

import com.badlogic.gdx.controllers.Controller;
import com.playdeezgames.common.Controller.AbstractController;
import com.playdeezgames.common.Controller.AbstractControllerFactory;

public class XBoxAbstractControllerFactory implements AbstractControllerFactory{

	@Override
	public AbstractController createAbstractController(int controller,
			Controller theController) {
		return new XBoxAbstractController(controller,theController);
	}

}
