package com.pdg.Nanobot.interfaces;

public interface FileBrowser {
	String getSaveFilename();
	String getOpenFilename();
}
