package com.pdg.Nanobot.interfaces;

import com.pdg.common.interfaces.TextureRegionDescriptor;

public interface SpriteDescriptor<T extends TextureRegionDescriptor> {
	T getRegion();
}
