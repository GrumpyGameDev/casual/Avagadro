package com.pdg.Nanobot.application;

import com.pdg.Nanobot.enums.NanobotStates;
import com.pdg.Nanobot.interfaces.FileBrowser;
import com.pdg.Nanobot.states.EditState;
import com.pdg.common.baseclasses.StateMachine;

public class NanobotStateMachine extends StateMachine<NanobotStates>{
	private FileBrowser fileBrowser;
	public NanobotStateMachine(FileBrowser fileBrowser){
		setState(NanobotStates.EDIT,new EditState(fileBrowser));
	}
	public FileBrowser getFileBrowser() {
		return fileBrowser;
	}
	public void setFileBrowser(FileBrowser fileBrowser) {
		this.fileBrowser = fileBrowser;
	}
	@Override
	public void create(){
		super.create();
		setCurrent(NanobotStates.EDIT);
	}
}
