package com.pdg.Nanobot.application;

import com.badlogic.gdx.ApplicationListener;
import com.pdg.Nanobot.interfaces.FileBrowser;

public class Nanobot implements ApplicationListener{
	private NanobotStateMachine machine;
	public Nanobot(FileBrowser fileBrowser){
		machine = new NanobotStateMachine(fileBrowser);
	}
	
	@Override
	public void create() {
		machine.create();
	}

	@Override
	public void dispose() {
		machine.dispose();
	}

	@Override
	public void render() {
		machine.render();
	}

	@Override
	public void resize(int width, int height) {
		machine.resize(width, height);
	}

	@Override
	public void pause() {
		machine.pause();
	}

	@Override
	public void resume() {
		machine.resume();
	}


}
