package com.pdg.Nanobot.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.pdg.Nanobot.board.BoardCell;
import com.pdg.Nanobot.enums.TextureRegions;
import com.pdg.common.managers.TextureRegionManager;

public class BoardCellActor extends Actor{
	private BoardCell boardCell;
	private TextureRegionManager<TextureRegions> regionManager;
	
	public BoardCellActor(TextureRegionManager<TextureRegions> regionManager,BoardCell boardCell){
		setRegionManager(regionManager);
		setBoardCell(boardCell);
	}

	public BoardCell getBoardCell() {
		return boardCell;
	}

	public void setBoardCell(BoardCell boardCell) {
		this.boardCell = boardCell;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha){
		batch.draw(getRegionManager().get(getBoardCell().getBackground().getRegion()),getX(),getY());
		batch.draw(getRegionManager().get(getBoardCell().getForeground().getRegion()),getX(),getY());
		if(getBoardCell().isCursor()){
			batch.draw(getRegionManager().get(TextureRegions.CURSOR),getX(),getY());
		}
	}

	public TextureRegionManager<TextureRegions> getRegionManager() {
		return regionManager;
	}

	public void setRegionManager(TextureRegionManager<TextureRegions> regionManager) {
		this.regionManager = regionManager;
	}
}
