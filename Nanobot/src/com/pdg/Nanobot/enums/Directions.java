package com.pdg.Nanobot.enums;

public enum Directions {
	NORTH,
	EAST,
	SOUTH,
	WEST;
	public int nextColumn(int startColumn,int startRow){
		switch(this){
		case EAST:
			return startColumn+1;
		case WEST:
			return startColumn-1;
		default:
			return startColumn;
		}
	}
	public int nextRow(int startColumn,int startRow){
		switch(this){
		case NORTH:
			return startRow+1;
		case SOUTH:
			return startRow-1;
		default:
			return startRow;
		}
	}
	public Directions opposite()
	{
		switch(this){
		case NORTH:
			return SOUTH;
		case SOUTH:
			return NORTH;
		case EAST:
			return WEST;
		case WEST:
			return EAST;
		default:
			return null;
		}
	}
}
