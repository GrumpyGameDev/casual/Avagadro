package com.pdg.Nanobot.enums;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.pdg.common.interfaces.TextureDescriptor;

public enum Textures implements TextureDescriptor  {
	NANOBOT_NORTH(Images.NANOBOT_NORTH,TextureFilter.Nearest,TextureFilter.Nearest),
	NANOBOT_EAST(Images.NANOBOT_EAST,TextureFilter.Nearest,TextureFilter.Nearest),
	NANOBOT_SOUTH(Images.NANOBOT_SOUTH,TextureFilter.Nearest,TextureFilter.Nearest),
	NANOBOT_WEST(Images.NANOBOT_WEST,TextureFilter.Nearest,TextureFilter.Nearest),
	FLOOR(Images.FLOOR,TextureFilter.Nearest,TextureFilter.Nearest),
	EMPTY(Images.EMPTY,TextureFilter.Nearest,TextureFilter.Nearest),
	TARGET(Images.TARGET,TextureFilter.Nearest,TextureFilter.Nearest),
	WALL(Images.WALL,TextureFilter.Nearest,TextureFilter.Nearest),
	HELIUM(Images.HELIUM,TextureFilter.Nearest,TextureFilter.Nearest),
	HYDROGEN_NORTH(Images.HYDROGEN_NORTH,TextureFilter.Nearest,TextureFilter.Nearest),
	HYDROGEN_EAST(Images.HYDROGEN_EAST,TextureFilter.Nearest,TextureFilter.Nearest),
	HYDROGEN_SOUTH(Images.HYDROGEN_SOUTH,TextureFilter.Nearest,TextureFilter.Nearest),
	HYDROGEN_WEST(Images.HYDROGEN_WEST,TextureFilter.Nearest,TextureFilter.Nearest),
	OXYGEN_NORTHEAST(Images.OXYGEN_NORTHEAST,TextureFilter.Nearest,TextureFilter.Nearest),
	OXYGEN_EASTSOUTH(Images.OXYGEN_EASTSOUTH,TextureFilter.Nearest,TextureFilter.Nearest),
	OXYGEN_SOUTHWEST(Images.OXYGEN_SOUTHWEST,TextureFilter.Nearest,TextureFilter.Nearest),
	OXYGEN_WESTNORTH(Images.OXYGEN_WESTNORTH,TextureFilter.Nearest,TextureFilter.Nearest),
	OXYGEN_NORTHSOUTH(Images.OXYGEN_NORTHSOUTH,TextureFilter.Nearest,TextureFilter.Nearest),
	OXYGEN_EASTWEST(Images.OXYGEN_EASTWEST,TextureFilter.Nearest,TextureFilter.Nearest),
	NITROGEN_NORTHEASTSOUTH(Images.NITROGEN_NORTHEASTSOUTH,TextureFilter.Nearest,TextureFilter.Nearest),
	NITROGEN_EASTSOUTHWEST(Images.NITROGEN_EASTSOUTHWEST,TextureFilter.Nearest,TextureFilter.Nearest),
	NITROGEN_SOUTHWESTNORTH(Images.NITROGEN_SOUTHWESTNORTH,TextureFilter.Nearest,TextureFilter.Nearest),
	NITROGEN_WESTNORTHEAST(Images.NITROGEN_WESTNORTHEAST,TextureFilter.Nearest,TextureFilter.Nearest),
	CARBON(Images.CARBON,TextureFilter.Nearest,TextureFilter.Nearest), 
	CURSOR(Images.CURSOR,TextureFilter.Nearest,TextureFilter.Nearest);

	private Images image;
	private TextureFilter minFilter;
	private TextureFilter magFilter;

	Textures(Images image,TextureFilter minFilter, TextureFilter magFilter){
		this.image=image;
		this.minFilter=minFilter;
		this.magFilter=magFilter;
	}
	
	@Override
	public String getFileName() {
		return image.getFileName();
	}

	@Override
	public TextureFilter getMinFilter() {
		return minFilter;
	}

	@Override
	public TextureFilter getMagFilter() {
		return magFilter;
	}

}
