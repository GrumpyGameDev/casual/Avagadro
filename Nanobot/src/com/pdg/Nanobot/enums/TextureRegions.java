package com.pdg.Nanobot.enums;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.pdg.common.interfaces.TextureRegionDescriptor;

public enum TextureRegions implements TextureRegionDescriptor{
	NANOBOT_NORTH(Textures.NANOBOT_NORTH,0,0,64,64),
	NANOBOT_EAST(Textures.NANOBOT_EAST,0,0,64,64),
	NANOBOT_SOUTH(Textures.NANOBOT_SOUTH,0,0,64,64),
	NANOBOT_WEST(Textures.NANOBOT_WEST,0,0,64,64),
	FLOOR(Textures.FLOOR,0,0,64,64),
	EMPTY(Textures.EMPTY,0,0,64,64),
	TARGET(Textures.TARGET,0,0,64,64),
	WALL(Textures.WALL,0,0,64,64),
	HELIUM(Textures.HELIUM,0,0,64,64),
	HYDROGEN_NORTH(Textures.HYDROGEN_NORTH,0,0,64,64),
	HYDROGEN_EAST(Textures.HYDROGEN_EAST,0,0,64,64),
	HYDROGEN_SOUTH(Textures.HYDROGEN_SOUTH,0,0,64,64),
	HYDROGEN_WEST(Textures.HYDROGEN_WEST,0,0,64,64),
	OXYGEN_NORTHEAST(Textures.OXYGEN_NORTHEAST,0,0,64,64),
	OXYGEN_EASTSOUTH(Textures.OXYGEN_EASTSOUTH,0,0,64,64),
	OXYGEN_SOUTHWEST(Textures.OXYGEN_SOUTHWEST,0,0,64,64),
	OXYGEN_WESTNORTH(Textures.OXYGEN_WESTNORTH,0,0,64,64),
	OXYGEN_NORTHSOUTH(Textures.OXYGEN_NORTHSOUTH,0,0,64,64),
	OXYGEN_EASTWEST(Textures.OXYGEN_EASTWEST,0,0,64,64),
	NITROGEN_NORTHEASTSOUTH(Textures.NITROGEN_NORTHEASTSOUTH,0,0,64,64),
	NITROGEN_EASTSOUTHWEST(Textures.NITROGEN_EASTSOUTHWEST,0,0,64,64),
	NITROGEN_SOUTHWESTNORTH(Textures.NITROGEN_SOUTHWESTNORTH,0,0,64,64),
	NITROGEN_WESTNORTHEAST(Textures.NITROGEN_WESTNORTHEAST,0,0,64,64),
	CARBON(Textures.CARBON,0,0,64,64), 
	CURSOR(Textures.CURSOR,0,0,64,64);
	private Textures texture;
	private int x;
	private int y;
	private int width;
	private int height;

	TextureRegions(Textures texture, int x,int y, int width, int height){
		this.texture=texture;
		this.x=x;
		this.y=y;
		this.width=width;
		this.height=height;
	}
	
	@Override
	public TextureFilter getMinFilter() {
		return texture.getMinFilter();
	}

	@Override
	public TextureFilter getMagFilter() {
		return texture.getMagFilter();
	}

	@Override
	public String getFileName() {
		return texture.getFileName();
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

}
