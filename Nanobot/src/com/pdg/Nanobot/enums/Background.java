package com.pdg.Nanobot.enums;

import com.pdg.Nanobot.interfaces.SpriteDescriptor;


public enum Background implements SpriteDescriptor<TextureRegions>{
	FLOOR(TextureRegions.FLOOR),
	TARGET(TextureRegions.TARGET);
	TextureRegions region;
	Background(TextureRegions region){
		this.region=region;
	}
	public TextureRegions getRegion(){
		return region;
	}
	public Background toggle(){
		switch(this){
		case FLOOR:
			return TARGET;
		case TARGET:
			return FLOOR;
		default:
			return null;
		}
	}
}
