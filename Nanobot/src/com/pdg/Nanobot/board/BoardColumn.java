package com.pdg.Nanobot.board;

import java.util.ArrayList;
import java.util.List;

import com.pdg.Nanobot.enums.Background;
import com.pdg.Nanobot.enums.Foreground;

public class BoardColumn {
	private List<BoardCell> boardCells = new ArrayList<BoardCell>();
	private Foreground foreground;
	private Background background;
	private int rows;
	private Board board;
	
	public BoardColumn(Board board, int rows,Foreground foreground,Background background) {
		setBoard(board);
		setBackground(background);
		setForeground(foreground);
		setRows(rows);
	}

	private void setBoard(Board board) {
		this.board=board;
	}
	
	public Board getBoard(){
		return board;
	}

	private void setForeground(Foreground foreground) {
		this.foreground=foreground;
	}

	private void setBackground(Background background) {
		this.background=background;
	}

	public void setRows(int rows) {
		this.rows=rows;
		update();
	}

	private void update() {
		while(rows<boardCells.size()){
			boardCells.remove(boardCells.size()-1);
		}
		while(rows>boardCells.size()){
			boardCells.add(new BoardCell(this,getForeground(),getBackground()));
		}
	}

	private Background getBackground() {
		return background;
	}

	private Foreground getForeground() {
		return foreground;
	}

	public BoardCell get(int y) {
		return boardCells.get(y);
	}

}
