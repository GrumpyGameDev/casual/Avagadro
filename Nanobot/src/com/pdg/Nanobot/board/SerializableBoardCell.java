package com.pdg.Nanobot.board;

import java.io.Serializable;

import com.pdg.Nanobot.enums.Background;
import com.pdg.Nanobot.enums.Foreground;

public class SerializableBoardCell implements Serializable {

	public SerializableBoardCell(BoardCell boardCell) {
		background=boardCell.getBackground();
		foreground=boardCell.getForeground();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = -5718282501397489226L;
	
	public Background background;
	public Foreground foreground;

}
