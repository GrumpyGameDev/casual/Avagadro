package com.pdg.Nanobot.board;

import java.io.Serializable;

public class SerializableBoard implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2977677442078435120L;
	
	public SerializableBoardCell[][] cells;
	
	public SerializableBoard(Board board){
		cells = new SerializableBoardCell[board.getColumns()][board.getRows()];
		for(int column=0;column<board.getColumns();++column){
			for(int row=0;row<board.getRows();++row){
				cells[column][row]=new SerializableBoardCell(board.get(column).get(row));
			}
		}
	}

}
