package com.pdg.common.interfaces;


public interface AssetDescriptor {
	String getFileName();
}
