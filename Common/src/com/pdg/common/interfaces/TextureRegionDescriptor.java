package com.pdg.common.interfaces;


public interface TextureRegionDescriptor extends TextureDescriptor{
	int getX();
	int getY();
	int getWidth();
	int getHeight();	
}
