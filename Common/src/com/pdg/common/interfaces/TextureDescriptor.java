package com.pdg.common.interfaces;

import com.badlogic.gdx.graphics.Texture.TextureFilter;

public interface TextureDescriptor extends AssetDescriptor {
	TextureFilter getMinFilter();
	TextureFilter getMagFilter();
}
