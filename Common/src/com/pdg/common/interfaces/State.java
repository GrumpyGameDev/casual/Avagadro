package com.pdg.common.interfaces;

public interface State{

	void dispose();

	void render();

	void resize(int width, int height);

	void pause();

	void resume();

	void create();

	void finish();

	void start();
}
