package com.pdg.common.managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.pdg.common.interfaces.AssetDescriptor;

public class AssetManager<T extends AssetDescriptor> {
	private Map<T,FileHandle> cache = new HashMap<T,FileHandle>();
	public FileHandle get(T key){
		if(cache.containsKey(key)){
			return cache.get(key);
		}else{
			FileHandle value = Gdx.files.internal(key.getFileName());
			cache.put(key, value);
			return value;
		}
	}
}
