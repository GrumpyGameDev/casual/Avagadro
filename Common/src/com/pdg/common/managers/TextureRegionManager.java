package com.pdg.common.managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.pdg.common.interfaces.TextureRegionDescriptor;


public class TextureRegionManager<T extends TextureRegionDescriptor> {
	private TextureManager<T> textureManager = new TextureManager<T>();
	private Map<T,TextureRegion> cache = new HashMap<T,TextureRegion>();
	public TextureRegion get(T key){
		if(cache.containsKey(key)){
			return cache.get(key);
		}else{
			TextureRegion value = new TextureRegion(textureManager.get(key),key.getX(),key.getY(),key.getWidth(),key.getHeight());
			cache.put(key, value);
			return value;
		}
	}
	public void dispose() {
		textureManager.dispose();
	}
}
