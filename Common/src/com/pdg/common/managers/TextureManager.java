package com.pdg.common.managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Texture;
import com.pdg.common.interfaces.TextureDescriptor;

public class TextureManager<T extends TextureDescriptor> {
	private AssetManager<T> assetManager = new AssetManager<T>();
	private Map<T,Texture> cache = new HashMap<T,Texture>();
	public Texture get(T key){
		if(cache.containsKey(key)){
			return cache.get(key);
		}else{
			Texture value = new Texture(assetManager.get(key));
			value.setFilter(key.getMinFilter(), key.getMagFilter());
			cache.put(key, value);
			return value;
		}
	}
	public void dispose() {
		for(Texture texture:cache.values()){
			texture.dispose();
		}
	}
}
