package com.pdg.common.ascii;

public class AsciiMenuItem {
	private boolean visible;
	private boolean enabled;
	private String text;
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	} 
	public AsciiMenuItem(String theText,boolean visible,boolean enabled){
		setText(theText);
		setVisible(visible);
		setEnabled(enabled);
	}
}
