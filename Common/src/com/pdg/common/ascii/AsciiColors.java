package com.pdg.common.ascii;

public enum AsciiColors {
	BLACK(0,0,0,1),
	BLUE(0,0,0.666666f,1),
	GREEN(0,0.666666f,0,1),
	CYAN(0,0.666666f,0.666666f,1),
	RED(0.666666f,0,0,1),
	MAGENTA(0.666666f,0,0.666666f,1),
	BROWN(0.666666f,0.333333f,0,1),
	LIGHT_GRAY(0.666666f,0.666666f,0.666666f,1),
	DARK_GRAY(0.333333f,0.333333f,0.333333f,1),
	LIGHT_BLUE(0.333333f,0.333333f,1,1),
	LIGHT_GREEN(0.333333f,1,0.333333f,1),
	LIGHT_CYAN(0.333333f,1,1,1),
	LIGHT_RED(1,0.333333f,0.333333f,1),
	LIGHT_MAGENTA(1,0.333333f,1,1),
	YELLOW(1,1,0.333333f,1),
	WHITE(1,1,1,1),
	TRANSPARENT(0,0,0,0);
	private float red;
	private float green;
	private float blue;
	private float alpha;
	AsciiColors(float theRed,float theGreen,float theBlue,float theAlpha){
		red=theRed;
		green=theGreen;
		blue = theBlue;
		alpha=theAlpha;
	}
	public float getRed(){
		return red;
	}
	public float getGreen(){
		return green;
	}
	public float getBlue(){
		return blue;
	}
	public float getAlpha(){
		return alpha;
	}
}
