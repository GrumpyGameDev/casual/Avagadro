package com.pdg.common.ascii;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pdg.common.managers.TextureRegionManager;

public class AsciiTextCell extends AsciiTextCellData{
	private TextureRegionManager<AsciiCharacters> regionManager;
	private Sprite backgroundSprite;
	private Sprite foregroundSprite;
	@Override
	public void setBackgroundColor(AsciiColors theBackgroundColor){
		super.setBackgroundColor(theBackgroundColor);
		backgroundSprite.setColor(theBackgroundColor.getRed(),theBackgroundColor.getGreen(),theBackgroundColor.getBlue(),theBackgroundColor.getAlpha());
	}
	@Override
	public void setForegroundColor(AsciiColors theForegroundColor){
		super.setForegroundColor(theForegroundColor);
		foregroundSprite.setColor(theForegroundColor.getRed(),theForegroundColor.getGreen(),theForegroundColor.getBlue(),theForegroundColor.getAlpha());
	}
	@Override
	public void setCharacter(AsciiCharacters theCharacter){
		super.setCharacter(theCharacter);
		foregroundSprite.setRegion(regionManager.get(getCharacter()));
	}
	public AsciiTextCell(TextureRegionManager<AsciiCharacters> theRegionManager,float x,float y,float width,float height){
		regionManager=theRegionManager;
		backgroundSprite = new Sprite(regionManager.get(AsciiCharacters.CHARACTER_219));
		backgroundSprite.setSize(width, height);
		backgroundSprite.setPosition(x, y);
		backgroundSprite.setOrigin(0,0);
		foregroundSprite = new Sprite();
		foregroundSprite.setSize(width, height);
		foregroundSprite.setPosition(x, y);
		foregroundSprite.setOrigin(0,0);
		setBackgroundColor(getBackgroundColor());
		setForegroundColor(getForegroundColor());
		setCharacter(getCharacter());
	}
	public void draw(SpriteBatch batch){
		backgroundSprite.draw(batch);
		foregroundSprite.draw(batch);
	}
}
