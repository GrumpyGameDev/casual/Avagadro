package com.pdg.common.ascii;

import java.util.ArrayList;
import java.util.List;

public class AsciiMenu {
	private List<AsciiMenuItem> items = new ArrayList<AsciiMenuItem>();
	private int hilite=0;
	private AsciiTextCellData normalCell = new AsciiTextCellData(AsciiCharacters.CHARACTER_0,AsciiColors.WHITE,AsciiColors.BLACK);
	private AsciiTextCellData hiliteCell = new AsciiTextCellData(AsciiCharacters.CHARACTER_0,AsciiColors.BLACK,AsciiColors.WHITE);
	private AsciiTextCellData disabledCell = new AsciiTextCellData(AsciiCharacters.CHARACTER_0,AsciiColors.DARK_GRAY,AsciiColors.BLACK);
	public List<AsciiMenuItem> getItems(){
		return items;
	}
	public AsciiTextCellData getNormalCell() {
		return normalCell;
	}
	public AsciiTextCellData getHiliteCell() {
		return hiliteCell;
	}
	public AsciiTextCellData getDisabledCell() {
		return disabledCell;
	}
	public void hiliteNext(){
		int count=getItems().size();
		int theHilite=getHilite();
		while(count>0){
			theHilite=(theHilite+1)%getItems().size();
			if(getItems().get(theHilite).isVisible() &&getItems().get(theHilite).isEnabled()){
				setHilite(theHilite);
				return;
			}
			count--;
		}
	}
	public void hilitePrevious(){
		int count=getItems().size();
		int theHilite=getHilite();
		while(count>0){
			theHilite=(theHilite+getItems().size()-1)%getItems().size();
			if(getItems().get(theHilite).isVisible() &&getItems().get(theHilite).isEnabled()){
				setHilite(theHilite);
				return;
			}
			count--;
		}
	}
	public void render(AsciiTextGrid theGrid,int column,int row){
		for(int index=0;index<getItems().size();++index){
			AsciiMenuItem item = getItems().get(index);
			AsciiTextCellData cellData = getDisabledCell(); 
			if(item.isVisible()){
				if(item.isEnabled()){
					if(index==getHilite()){
						cellData = getHiliteCell();
					}else{
						cellData=getNormalCell();
					}
				}
				theGrid.WriteText(column, row, item.getText(), cellData.getForegroundColor(), cellData.getBackgroundColor());
				row--;
			}
		}
	}
	public int getHilite() {
		return hilite;
	}
	public void setHilite(int hilite) {
		this.hilite = hilite;
	}
}
