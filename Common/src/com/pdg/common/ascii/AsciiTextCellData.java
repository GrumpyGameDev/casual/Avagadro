package com.pdg.common.ascii;

public class AsciiTextCellData {
	private AsciiColors backgroundColor = AsciiColors.BLACK;
	private AsciiColors foregroundColor = AsciiColors.BLACK;
	private AsciiCharacters character=AsciiCharacters.CHARACTER_0;
	public AsciiColors getBackgroundColor(){
		return backgroundColor;
	}
	public AsciiColors getForegroundColor(){
		return foregroundColor;
	}
	public AsciiCharacters getCharacter(){
		return character;
	}
	public void setBackgroundColor(AsciiColors theBackgroundColor){
		if(theBackgroundColor!=null){
			backgroundColor=theBackgroundColor;
		}
	}
	public void setForegroundColor(AsciiColors theForegroundColor){
		if(theForegroundColor!=null){
			foregroundColor=theForegroundColor;
		}
	}
	public void setCharacter(AsciiCharacters theCharacter){
		if(theCharacter!=null){
			character = theCharacter;
		}
	}
	public void set(AsciiTextCellData other){
		setBackgroundColor(other.getBackgroundColor());
		setForegroundColor(other.getForegroundColor());
		setCharacter(other.getCharacter());
	}
	public void set(AsciiCharacters theCharacter,AsciiColors theForegroundColor,AsciiColors theBackgroundColor){
		setCharacter(theCharacter);
		setForegroundColor(theForegroundColor);
		setBackgroundColor(theBackgroundColor);
	}
	public AsciiTextCellData(){
		
	}
	public AsciiTextCellData(AsciiCharacters theCharacter,AsciiColors theForegroundColor,AsciiColors theBackgroundColor){
		set(theCharacter,theForegroundColor,theBackgroundColor);
	}
	public AsciiTextCellData(AsciiTextCellData other){
		set(other);
	}
	
}
