package com.pdg.common.ascii;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pdg.common.managers.TextureRegionManager;

public class AsciiTextRow {
	private List<AsciiTextCell> cells = new ArrayList<AsciiTextCell>();
	public AsciiTextCell getCell(int column){
		return cells.get(column);
	}
	public AsciiTextRow(TextureRegionManager<AsciiCharacters> theRegionManager,
			float x,float y,float cellWidth,float cellHeight,int columns){
		while(cells.size()<columns){
			cells.add(new AsciiTextCell(theRegionManager,x,y,cellWidth,cellHeight));
			x+=cellWidth;
		}
	}
	public void draw(SpriteBatch batch){
		for(AsciiTextCell cell: cells){
			cell.draw(batch);
		}
	}
	public void WriteText(int column,String text,AsciiColors theForegroundColor,AsciiColors theBackgroundColor){
		for(int index=0;index<text.length();++index){
			if(column>=0 && column<cells.size()){
				getCell(column).setCharacter(AsciiCharacters.values()[text.charAt(index)]);
				getCell(column).setBackgroundColor(theBackgroundColor);
				getCell(column).setForegroundColor(theForegroundColor);
			}
			column++;
		}
	}
	public void fill(int column,int width,AsciiCharacters theCharacter,AsciiColors theForegroundColor,AsciiColors theBackgroundColor){
		while(width>0){
			if(column>=0 && column<cells.size()){
				getCell(column).set(theCharacter, theForegroundColor, theBackgroundColor);
			}
			column++;
			width--;
		}
	}
	public void fill(int column,int width,AsciiTextCellData cellData){
		fill(column,width,cellData.getCharacter(),cellData.getForegroundColor(),cellData.getBackgroundColor());
	}
	public void fill(AsciiCharacters theCharacter,AsciiColors theForegroundColor,AsciiColors theBackgroundColor){
		fill(0,getWidth(),theCharacter,theForegroundColor,theBackgroundColor);
	}
	public void fill(AsciiTextCellData cellData){
		fill(cellData.getCharacter(),cellData.getForegroundColor(),cellData.getBackgroundColor());
	}
	public int getWidth() {
		return cells.size();
	}
}
