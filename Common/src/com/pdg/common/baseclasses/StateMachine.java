package com.pdg.common.baseclasses;

import java.util.HashMap;
import java.util.Map;

import com.pdg.common.interfaces.State;

public class StateMachine<T> {
	private Map<T,State> states = new HashMap<T,State>(); 
	private T current;
	public void setState(T theId,State theState){
		states.put(theId, theState);
	}
	public State getState(T theId){
		return states.get(theId);
	}
	public void setCurrent(T theId){
		State currentState = getState(getCurrent());
		if(currentState!=null){
			currentState.finish();
		}
		current = theId;
		currentState = getState(getCurrent());
		if(currentState!=null){
			currentState.start();
		}
	}
	public T getCurrent(){
		return current;
	}
	public void create(){
		for(State state : states.values()){
			state.create();
		}
	}
	public void dispose() {
		for(State state : states.values()){
			state.dispose();
		}
	}

	public void render() {
		getState(getCurrent()).render();
	}

	public void resize(int width, int height) {
		for(State state : states.values()){
			state.resize(width,height);
		}
	}

	public void pause() {
		getState(getCurrent()).pause();
	}

	public void resume() {
		getState(getCurrent()).resume();
	}	
}

