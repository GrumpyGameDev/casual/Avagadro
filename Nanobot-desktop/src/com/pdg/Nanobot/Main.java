package com.pdg.Nanobot;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.pdg.Nanobot.application.Nanobot;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Nanobot";
		cfg.useGL20 = false;
		cfg.width = 768;
		cfg.height = 576;
		
		new LwjglApplication(new Nanobot(new DesktopFileBrowser()), cfg);
	}
}
