package com.pdg.Nanobot;

import javax.swing.JFileChooser;

import com.pdg.Nanobot.interfaces.FileBrowser;

public class DesktopFileBrowser implements FileBrowser{

	@Override
	public String getSaveFilename() {
		JFileChooser chooser=new JFileChooser();
		int result = chooser.showSaveDialog(null);
		if(result==JFileChooser.APPROVE_OPTION){
			return chooser.getSelectedFile().getAbsolutePath();
		}else{
			return null;
		}
	}

	@Override
	public String getOpenFilename() {
		JFileChooser chooser=new JFileChooser();
		int result = chooser.showOpenDialog(null);
		if(result==JFileChooser.APPROVE_OPTION){
			return chooser.getSelectedFile().getAbsolutePath();
		}else{
			return null;
		}
	}

}
