package com.playdeezgames.Avagadro;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import com.playdeezgames.common.Controller.AbstractControllerBase;
import com.playdeezgames.common.Controller.AbstractControllerListener;
import com.playdeezgames.common.Controller.ControllerAxis;
import com.playdeezgames.common.Controller.ControllerButton;
import com.playdeezgames.common.Controller.ControllerButtonState;
import com.playdeezgames.common.Controller.ControllerDPadState;

public class OuyaAbstractController extends AbstractControllerBase implements ControllerListener{

	public OuyaAbstractController(int theId,Controller theController) {
		super(theId);
		theController.addListener(this);
	}
	
	private boolean left=false;
	private boolean right=false;
	private boolean up=false;
	private boolean down=false;

	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		return false;
	}

	@Override
	public boolean axisMoved(Controller arg0, int arg1, float arg2) {
		ControllerAxis theAxis=null;
		float theAxisValue = arg2;
		switch(arg1){
		case 0:
			theAxis = ControllerAxis.LEFT_STICK_X;
			break;
		case 1:
			theAxis = ControllerAxis.LEFT_STICK_Y;
			break;
		case 2:
			theAxis = ControllerAxis.LEFT_TRIGGER;
			break;
		case 3:
			theAxis = ControllerAxis.RIGHT_STICK_X;
			break;
		case 4:
			theAxis = ControllerAxis.RIGHT_STICK_Y;
			break;
		case 5:
			theAxis = ControllerAxis.RIGHT_TRIGGER;
			break;
		default:
			return false;
		}
		for(AbstractControllerListener listener:listeners){
			listener.onAxisChange(getId(), theAxis, theAxisValue);
		}
		return true;
	}

	@Override
	public boolean buttonDown(Controller arg0, int arg1) {
		ControllerButton theButton;
		switch(arg1){
		case 19:
			up=true;
			updateDPad();
			return true;
		case 20:
			down=true;
			updateDPad();
			return true;
		case 21:
			left=true;
			updateDPad();
			return true;
		case 22:
			right=true;
			updateDPad();
			return true;
		case 82:
			theButton=ControllerButton.PAUSE;
			break;
		case 96:
			theButton=ControllerButton.GREEN;
			break;
		case 97:
			theButton=ControllerButton.RED;
			break;
		case 99:
			theButton=ControllerButton.BLUE;
			break;
		case 100:
			theButton=ControllerButton.YELLOW;
			break;
		case 102:
			theButton=ControllerButton.LEFT_SHOULDER;
			break;
		case 103:
			theButton=ControllerButton.RIGHT_SHOULDER;
			break;
		case 106:
			theButton=ControllerButton.LEFT_STICK;
			break;
		case 107:
			theButton=ControllerButton.RIGHT_STICK;
			break;
		default:
			return false;
		}
		for(AbstractControllerListener listener:listeners){
			listener.onButtonChange(getId(), theButton, ControllerButtonState.PRESSED);
		}
		return true;
	}

	private void updateDPad() {
		ControllerDPadState theState = ControllerDPadState.CENTER;
		if(left){
			if(up){
				theState = ControllerDPadState.NORTHWEST;
			}else if(down){
				theState = ControllerDPadState.SOUTHWEST;
			}else{
				theState = ControllerDPadState.WEST;
			}
		}else if(right){
			if(up){
				theState = ControllerDPadState.NORTHEAST;
			}else if(down){
				theState = ControllerDPadState.SOUTHEAST;
			}else{
				theState = ControllerDPadState.EAST;
			}
		}else if(up){
			theState = ControllerDPadState.NORTH;
		}else if(down){
			theState = ControllerDPadState.SOUTH;
		}
		for(AbstractControllerListener listener:listeners){
			listener.onDPadChange(getId(), theState);
		}
	}

	@Override
	public boolean buttonUp(Controller arg0, int arg1) {
		ControllerButton theButton;
		switch(arg1){
		case 19:
			up=false;
			updateDPad();
			return true;
		case 20:
			down=false;
			updateDPad();
			return true;
		case 21:
			left=false;
			updateDPad();
			return true;
		case 22:
			right=false;
			updateDPad();
			return true;
		case 82:
			theButton=ControllerButton.PAUSE;
			break;
		case 96:
			theButton=ControllerButton.GREEN;
			break;
		case 97:
			theButton=ControllerButton.RED;
			break;
		case 99:
			theButton=ControllerButton.BLUE;
			break;
		case 100:
			theButton=ControllerButton.YELLOW;
			break;
		case 102:
			theButton=ControllerButton.LEFT_SHOULDER;
			break;
		case 103:
			theButton=ControllerButton.RIGHT_SHOULDER;
			break;
		case 106:
			theButton=ControllerButton.LEFT_STICK;
			break;
		case 107:
			theButton=ControllerButton.RIGHT_STICK;
			break;
		default:
			return false;
		}
		for(AbstractControllerListener listener:listeners){
			listener.onButtonChange(getId(), theButton, ControllerButtonState.RELEASED);
		}
		return true;
	}

	@Override
	public void connected(Controller arg0) {
	}

	@Override
	public void disconnected(Controller arg0) {
	}

	@Override
	public boolean povMoved(Controller arg0, int arg1, PovDirection arg2) {
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller arg0, int arg1, boolean arg2) {
		return false;
	}

}
