package com.pdg.common;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


public class TextureRegionManager<T extends TextureRegionDescriptor> {
	private TextureManager<T> textureManager = new TextureManager<T>();
	private Map<T,TextureRegion> cache = new HashMap<T,TextureRegion>();
	private Map<T,TextureRegionDrawable> drawableCache = new HashMap<T,TextureRegionDrawable>();
	public TextureRegion get(T key){
		if(cache.containsKey(key)){
			return cache.get(key);
		}else{
			TextureRegion value = new TextureRegion(textureManager.get(key),key.getX(),key.getY(),key.getWidth(),key.getHeight());
			cache.put(key, value);
			return value;
		}
	}
	public TextureRegionDrawable getDrawable(T key){
		if(drawableCache.containsKey(key)){
			return drawableCache.get(key);
		}else{
			TextureRegionDrawable drawable = new TextureRegionDrawable(get(key));
			drawableCache.put(key, drawable);
			return drawable;
		}
	}
}
