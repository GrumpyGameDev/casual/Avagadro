package com.pdg.Nanobot;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Nanobot implements ApplicationListener, InputProcessor {
	private Stage stage;
	private Board board;
	private Group boardGroup;
	private FileBrowser fileBrowser;
	private TextureRegionManager<TextureRegions> manager;
	public Nanobot(FileBrowser fileBrowser){
		setFileBrowser(fileBrowser);
	}
	
	private void setFileBrowser(FileBrowser fileBrowser) {
		this.fileBrowser=fileBrowser;
	}

	private static final float CELL_WIDTH=64.0f;
	private static final float CELL_HEIGHT=64.0f;
	private static final int BOARD_INITIAL_WIDTH=12;
	private static final int BOARD_INITIAL_HEIGHT=9;
	
	@Override
	public void create() {
		Gdx.input.setInputProcessor(this);
		board = new Board(BOARD_INITIAL_WIDTH,BOARD_INITIAL_HEIGHT,Foreground.EMPTY,Background.FLOOR);
		manager = new TextureRegionManager<TextureRegions>();
		recreateStage();
	}

	private void recreateStage() {
		float scale=1.0f;
		if(boardGroup!=null){
			scale = boardGroup.getScaleX();
		}
		stage = new Stage(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),true);
		board.setCursor(board.get(0).get(0));
		boardGroup = new Group();
		boardGroup.setWidth(CELL_WIDTH*board.getColumns());
		boardGroup.setHeight(CELL_HEIGHT*board.getRows());
		for(int row=0;row<board.getRows();++row){
			for(int column=0;column<board.getColumns();++column){
				BoardCellActor actor = new BoardCellActor(manager,board.get(column).get(row));
				boardGroup.addActor(actor);
				actor.setBounds(CELL_WIDTH*column, CELL_HEIGHT*row, CELL_WIDTH, CELL_HEIGHT);
			}
		}
		boardGroup.setScale(scale);
		stage.addActor(boardGroup);
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	@Override
	public void render() {
		stage.act(Gdx.graphics.getDeltaTime());
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	private FileBrowser getFileBrowser() {
		return fileBrowser;
	}

	@Override
	public boolean keyDown(int keycode) {
		if(keycode==Input.Keys.S && getFileBrowser()!=null){
			String fileName = getFileBrowser().getSaveFilename();
			if(fileName==null) return true;
			SerializableBoard outBoard = new SerializableBoard(board);
			FileOutputStream fileOut;
			try {
				fileOut = new FileOutputStream(fileName);
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeObject(outBoard);
			         out.close();
			         fileOut.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}else if(keycode==Input.Keys.L && getFileBrowser()!=null){
			String fileName = getFileBrowser().getOpenFilename();
			if(fileName==null) return true;
			SerializableBoard inBoard;
			try
		      {
		         FileInputStream fileIn = new FileInputStream(fileName);
		         ObjectInputStream in = new ObjectInputStream(fileIn);
		         inBoard = (SerializableBoard) in.readObject();
		         board = new Board(inBoard,Foreground.EMPTY,Background.FLOOR);
		         recreateStage();
		         in.close();
		         fileIn.close();
		      }catch(IOException i)
		      {
		         i.printStackTrace();
		      }catch(ClassNotFoundException c)
		      {
		         c.printStackTrace();
		      }		
			}else if(keycode==Input.Keys.UP){
			BoardCell nextCursor=board.getCursor().getNeighbor(Directions.NORTH);
			if(nextCursor!=null){
				board.setCursor(nextCursor);
			}
			return true;
		}else if(keycode==Input.Keys.DOWN){
			BoardCell nextCursor=board.getCursor().getNeighbor(Directions.SOUTH);
			if(nextCursor!=null){
				board.setCursor(nextCursor);
			}
			return true;
		}else if(keycode==Input.Keys.Q){
			Gdx.app.exit();
			return true;
		}else if(keycode==Input.Keys.HOME){
			if(board.getColumns()>1){
				board.setColumns(board.getColumns()-1);
				recreateStage();
			}
			return true;
		}else if(keycode==Input.Keys.END){
			board.setColumns(board.getColumns()+1);
			recreateStage();
			return true;
		}else if(keycode==Input.Keys.PAGE_DOWN){
			if(board.getRows()>1){
				board.setRows(board.getRows()-1);
				recreateStage();
			}
			return true;
		}else if(keycode==Input.Keys.PAGE_UP){
			board.setRows(board.getRows()+1);
			recreateStage();
			return true;
		}else if(keycode==Input.Keys.PLUS){
			boardGroup.setScale(boardGroup.getScaleX()*1.1f);
			return true;
		}else if(keycode==Input.Keys.MINUS){
			boardGroup.setScale(boardGroup.getScaleX()/1.1f);
			return true;
		}else if(keycode==Input.Keys.LEFT){
			BoardCell nextCursor=board.getCursor().getNeighbor(Directions.WEST);
			if(nextCursor!=null){
				board.setCursor(nextCursor);
			}
			return true;
		}else if(keycode==Input.Keys.RIGHT){
			BoardCell nextCursor=board.getCursor().getNeighbor(Directions.EAST);
			if(nextCursor!=null){
				board.setCursor(nextCursor);
			}
			return true;
		}else if(keycode==Input.Keys.W){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.WALL);
			cursor.setBackground(Background.FLOOR);
			return true;
		}else if(keycode==Input.Keys.X){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.EMPTY);
			return true;
		}else if(keycode==Input.Keys.T){
			BoardCell cursor=board.getCursor();
			if(cursor.getForeground()!=Foreground.WALL){
				cursor.setBackground(cursor.getBackground().toggle());
			}
			return true;
		}else if(keycode==Input.Keys.R){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(cursor.getForeground().rotate());
			return true;
		}else if(keycode==Input.Keys.E){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.HELIUM);
			return true;
		}else if(keycode==Input.Keys.H){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.HYDROGEN_NORTH);
			return true;
		}else if(keycode==Input.Keys.O){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.OXYGEN_NORTHEAST);
			return true;
		}else if(keycode==Input.Keys.I){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.OXYGEN_NORTHSOUTH);
			return true;
		}else if(keycode==Input.Keys.N){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.NITROGEN_WESTNORTHEAST);
			return true;
		}else if(keycode==Input.Keys.C){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.CARBON);
			return true;
		}else if(keycode==Input.Keys.B){
			BoardCell cursor=board.getCursor();
			cursor.setForeground(Foreground.NANOBOT_NORTH);
			return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
