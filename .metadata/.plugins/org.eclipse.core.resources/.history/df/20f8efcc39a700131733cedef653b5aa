package com.pdg.Nanobot.board;

import java.util.ArrayList;
import java.util.List;

import com.pdg.Nanobot.SerializableBoard;
import com.pdg.Nanobot.enums.Background;
import com.pdg.Nanobot.enums.Directions;
import com.pdg.Nanobot.enums.Foreground;

public class Board {
	private int rows;
	private int columns;
	private List<BoardColumn> boardColumns = new ArrayList<BoardColumn>();
	private Foreground foreground;
	private Background background;
	private BoardCell cursor = null;

	public Board(int columns,int rows,Foreground foreground,Background background){
		setBackground(background);
		setForeground(foreground);
		setColumns(columns);
		setRows(rows);
	}

	public Board(SerializableBoard inBoard,Foreground foreground,Background background) {
		setBackground(background);
		setForeground(foreground);
		setColumns(inBoard.cells.length);
		setRows(inBoard.cells[0].length);
		for(int column=0;column<getColumns();++column){
			for(int row=0;row<getRows();++row){
				BoardCell cell = this.get(column).get(row);
				cell.setBackground(inBoard.cells[column][row].background);
				cell.setForeground(inBoard.cells[column][row].foreground);
			}
		}
	}

	private void setForeground(Foreground foreground) {
		this.foreground=foreground;
	}

	private void setBackground(Background background) {
		this.background=background;
	}

	public void setRows(int rows) {
		this.rows=rows;
		updateBoardSize();
	}

	public void setColumns(int columns) {
		this.columns=columns;
		updateBoardSize();
	}

	private void updateBoardSize() {
		while(columns<boardColumns.size()){
			boardColumns.remove(boardColumns.size()-1);
		}
		while(columns>boardColumns.size()){
			boardColumns.add(new BoardColumn(this,rows,getForeground(),getBackground()));
		}
		for(BoardColumn boardColumn:boardColumns){
			boardColumn.setRows(rows);
		}
		normalizeNeighbors();
	}

	private void normalizeNeighbors() {
		for(int column=0;column<getColumns();++column){
			for(int row=0;row<getRows();++row){
				BoardCell cell = this.get(column).get(row);
				for(Directions direction: Directions.values()){
					int nextColumn=direction.nextColumn(column, row);
					int nextRow=direction.nextRow(column, row);
					if(nextColumn>=0 && nextColumn<getColumns() && nextRow>=0 && nextRow<getRows()){
						BoardCell nextCell = this.get(nextColumn).get(nextRow);
						cell.setNeighbor(direction, nextCell);
					}else{
						cell.setNeighbor(direction, null);
					}
				}
			}
		}
	}

	private Background getBackground() {
		return background;
	}

	private Foreground getForeground() {
		return foreground;
	}

	public int getColumns() {
		return columns;
	}

	public int getRows() {
		return rows;
	}

	public BoardColumn get(int x) {
		return boardColumns.get(x);
	}

	public BoardCell getCursor() {
		return cursor;
	}

	public void setCursor(BoardCell cursor) {
		this.cursor = cursor;
	}
}
