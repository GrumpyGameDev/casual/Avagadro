package com.playdeezgames.Avagadro;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Music.OnCompletionListener;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.playdeezgames.Avagadro.Board.AvagadroBoard;
import com.playdeezgames.Avagadro.Enums.AvagadroMusic;
import com.playdeezgames.Avagadro.Enums.AvagadroStates;
import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;
import com.playdeezgames.Avagadro.Enums.AvagadroTextures;
import com.playdeezgames.Avagadro.States.AboutState;
import com.playdeezgames.Avagadro.States.ConfirmAbandonState;
import com.playdeezgames.Avagadro.States.ConfirmQuitState;
import com.playdeezgames.Avagadro.States.GoodByeState;
import com.playdeezgames.Avagadro.States.HowToPlayState;
import com.playdeezgames.Avagadro.States.MainMenuState;
import com.playdeezgames.Avagadro.States.OptionsState;
import com.playdeezgames.Avagadro.States.PlayState;
import com.playdeezgames.Avagadro.States.SplashState;
import com.playdeezgames.common.Ascii.AsciiTextGrid;
import com.playdeezgames.common.Ascii.AsciiCharacters;
import com.playdeezgames.common.Ascii.AsciiTextures;
import com.playdeezgames.common.Controller.AbstractController;
import com.playdeezgames.common.Controller.AbstractControllerFactory;
import com.playdeezgames.common.Controller.AbstractControllerListener;
import com.playdeezgames.common.Controller.ControllerAxis;
import com.playdeezgames.common.Controller.ControllerButton;
import com.playdeezgames.common.Controller.ControllerButtonState;
import com.playdeezgames.common.Controller.ControllerDPadState;
import com.playdeezgames.common.Generators.StandardWeightedGenerator;
import com.playdeezgames.common.Generators.WeightedGenerator;
import com.playdeezgames.common.Managers.MusicManager;
import com.playdeezgames.common.Managers.TextureManager;
import com.playdeezgames.common.Managers.TextureRegionManager;
import com.playdeezgames.common.StateMachine.StateMachine;

public class AvagadroStateMachine extends StateMachine<AvagadroStates> implements AbstractControllerListener, InputProcessor, OnCompletionListener{

	private static final float VIEWPORT_WIDTH=1280;
	private static final float VIEWPORT_HEIGHT=720;
	private static final float VIEWPORT_DEAD_ZONE_MULTIPLIER=0.8f;
	private static final int ASCII_GRID_COLUMNS = 48;
	private static final int ASCII_GRID_ROWS = 27;
	private static final float ASCII_CELL_WIDTH = (VIEWPORT_WIDTH*VIEWPORT_DEAD_ZONE_MULTIPLIER)/ASCII_GRID_COLUMNS;
	private static final float ASCII_CELL_HEIGHT = (VIEWPORT_HEIGHT*VIEWPORT_DEAD_ZONE_MULTIPLIER)/ASCII_GRID_ROWS;
	private static final float ASCII_GRID_X = -ASCII_CELL_WIDTH*ASCII_GRID_COLUMNS/2;
	private static final float ASCII_GRID_Y = -ASCII_CELL_HEIGHT*ASCII_GRID_ROWS/2;
	private static final int BOARD_COLUMNS = 9;
	private static final int BOARD_ROWS = 9;
	private static final float BOARD_CELL_WIDTH=72;
	private static final float BOARD_CELL_HEIGHT=72;
	private static final float BOARD_X = -BOARD_CELL_WIDTH*BOARD_COLUMNS/2;
	private static final float BOARD_Y = -BOARD_CELL_HEIGHT*BOARD_ROWS/2;
	private static final int BOARD_SLOTS = 8;
	private static final float SLOT_WIDTH = 36;
	private static final float SLOT_HEIGHT = 36;
	private static final float SLOTS_X = VIEWPORT_WIDTH*VIEWPORT_DEAD_ZONE_MULTIPLIER/2-SLOT_WIDTH;
	private static final float SLOTS_Y = VIEWPORT_HEIGHT*VIEWPORT_DEAD_ZONE_MULTIPLIER/2-SLOT_HEIGHT;
	private static final float SLOTS_DELTA_X = 0;
	private static final float SLOTS_DELTA_Y = -SLOT_HEIGHT;

	private OrthographicCamera camera;
	private SpriteBatch spriteBatch;
	private List<AbstractController> abstractControllers = new ArrayList<AbstractController>();
	private MusicManager<AvagadroMusic> musicManager;
	private WeightedGenerator<AvagadroMusic> musicGenerator;
	private TextureManager<AvagadroTextures> textureManager;
	private TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> textureRegionManager;
	private TextureRegionManager<AsciiTextures,AsciiCharacters> asciiTextureRegionManager;
	private TextureManager<AsciiTextures> asciiTextureManager;
	private AsciiTextGrid grid;
	private AbstractControllerFactory abstractControllerFactory;
	private AvagadroBoard board=null;
	private boolean upKey;
	private boolean downKey;
	private boolean leftKey;
	private boolean rightKey;
	
	public MusicManager<AvagadroMusic> getMusicManager(){
		if(musicManager==null){
			musicManager = new MusicManager<AvagadroMusic>(this);
		}
		return musicManager;
	}
	
	public WeightedGenerator<AvagadroMusic> getMusicGenerator(){
		if(musicGenerator==null){
			musicGenerator=new StandardWeightedGenerator<AvagadroMusic>(null);
			for(AvagadroMusic music:AvagadroMusic.values()){
				musicGenerator.setWeight(music, music.getWeight());
			}
		}
		return musicGenerator;
	}

	public AsciiTextGrid getAsciiTextGrid(){
		if(grid==null){
			grid = new AsciiTextGrid(getAsciiTextureRegionManager(), ASCII_GRID_X, ASCII_GRID_Y, ASCII_CELL_WIDTH, ASCII_CELL_HEIGHT, ASCII_GRID_COLUMNS, ASCII_GRID_ROWS);
		}
		return grid;
	}
	
	public OrthographicCamera getCamera(){
		if(camera==null){
			camera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
		}
		return camera;
	}
	
	public SpriteBatch getSpriteBatch(){
		if(spriteBatch==null){
			spriteBatch = new SpriteBatch();
		}
		return spriteBatch;
	}
	
	public TextureManager<AvagadroTextures> getTextureManager(){
		if(textureManager==null){
			textureManager = new TextureManager<AvagadroTextures>();
		}
		return textureManager;
	}
	
	public TextureManager<AsciiTextures> getAsciiTextureManager(){
		if(asciiTextureManager==null){
			asciiTextureManager = new TextureManager<AsciiTextures>();
		}
		return asciiTextureManager;
	}
	
	public TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> getTextureRegionManager(){
		if(textureRegionManager==null){
			textureRegionManager = new TextureRegionManager<AvagadroTextures,AvagadroTextureRegions>(getTextureManager());
		}
		return textureRegionManager;
	}
	
	public TextureRegionManager<AsciiTextures,AsciiCharacters> getAsciiTextureRegionManager(){
		if(asciiTextureRegionManager==null){
			asciiTextureRegionManager=new TextureRegionManager<AsciiTextures,AsciiCharacters>(getAsciiTextureManager());
		}
		return asciiTextureRegionManager;
	}
	
	public AvagadroStateMachine(AbstractControllerFactory theAbstractControllerFactory){
		abstractControllerFactory = theAbstractControllerFactory;
		setState(AvagadroStates.SPLASH,new SplashState(this));
		setState(AvagadroStates.MAIN_MENU,new MainMenuState(this));
		setState(AvagadroStates.ABOUT,new AboutState(this));
		setState(AvagadroStates.CONFIRM_QUIT,new ConfirmQuitState(this));
		setState(AvagadroStates.GOOD_BYE,new GoodByeState(this));
		setState(AvagadroStates.HOW_TO_PLAY,new HowToPlayState(this));
		setState(AvagadroStates.OPTIONS,new OptionsState(this));
		setState(AvagadroStates.PLAY,new PlayState(this));
		setState(AvagadroStates.CONFIRM_ABANDON,new ConfirmAbandonState(this));
		setCurrent(AvagadroStates.SPLASH);
	}

	@Override
	public void onAxisChange(int controller, ControllerAxis axis, float value) {
		getState(getCurrent()).onAxisChange(controller, axis, value);
	}

	@Override
	public void onButtonChange(int controller, ControllerButton button,
			ControllerButtonState state) {
		getState(getCurrent()).onButtonChange(controller, button, state);
	}

	@Override
	public void onDPadChange(int controller, ControllerDPadState state) {
		getState(getCurrent()).onDPadChange(controller, state);
	}
	
	@Override
	public void create(){
		super.create();
		int index=0;
		for(Controller controller: Controllers.getControllers()){
			AbstractController theAbstractController = abstractControllerFactory.createAbstractController(index, controller); 
			abstractControllers.add(theAbstractController);
			theAbstractController.addListener(this);
			++index;
		}
		if(index==0){
			Gdx.input.setInputProcessor(this);
		}
		getMusicManager().play(getMusicGenerator().generate());
	}
	
	@Override
	public void dispose() {
		getTextureManager().dispose();
		getAsciiTextureManager().dispose();
		getSpriteBatch().dispose();
		super.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		switch(keycode){
		case Input.Keys.UP:
			upKey=true;
			handleDPad();
			break;
		case Input.Keys.DOWN:
			downKey=true;
			handleDPad();
			break;
		case Input.Keys.LEFT:
			leftKey=true;
			handleDPad();
			break;
		case Input.Keys.RIGHT:
			rightKey=true;
			handleDPad();
			break;
		case Input.Keys.SPACE:
			onButtonChange(0, ControllerButton.GREEN, ControllerButtonState.PRESSED);
			break;
		case Input.Keys.ENTER:
			onButtonChange(0, ControllerButton.YELLOW, ControllerButtonState.PRESSED);
			break;
		case Input.Keys.ESCAPE:
			onButtonChange(0, ControllerButton.RED, ControllerButtonState.PRESSED);
			break;
		case Input.Keys.TAB:
			onButtonChange(0, ControllerButton.BLUE, ControllerButtonState.PRESSED);
			break;
		case Input.Keys.F10:
			onButtonChange(0, ControllerButton.PAUSE, ControllerButtonState.PRESSED);
			break;
		case Input.Keys.PERIOD:
		case Input.Keys.X:
			onButtonChange(0, ControllerButton.RIGHT_SHOULDER, ControllerButtonState.PRESSED);
			break;
		case Input.Keys.COMMA:
		case Input.Keys.Z:
			onButtonChange(0, ControllerButton.LEFT_SHOULDER, ControllerButtonState.PRESSED);
			break;
		default:
			return false;
		}
		return true;
	}

	private void handleDPad() {
		if(leftKey){
			if(upKey){
				this.onDPadChange(0, ControllerDPadState.NORTHWEST);
			}else if(downKey){
				this.onDPadChange(0, ControllerDPadState.SOUTHWEST);
			}else{
				this.onDPadChange(0, ControllerDPadState.WEST);
			}
		}else if(rightKey){
			if(upKey){
				this.onDPadChange(0, ControllerDPadState.NORTHEAST);
			}else if(downKey){
				this.onDPadChange(0, ControllerDPadState.SOUTHEAST);
			}else{
				this.onDPadChange(0, ControllerDPadState.EAST);
			}
		}else if(upKey){
			this.onDPadChange(0, ControllerDPadState.NORTH);
		}else if(downKey){
			this.onDPadChange(0, ControllerDPadState.SOUTH);
		}else{
			this.onDPadChange(0, ControllerDPadState.CENTER);
		}
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode){
		case Input.Keys.UP:
			upKey=false;
			handleDPad();
			break;
		case Input.Keys.DOWN:
			downKey=false;
			handleDPad();
			break;
		case Input.Keys.LEFT:
			leftKey=false;
			handleDPad();
			break;
		case Input.Keys.RIGHT:
			rightKey=false;
			handleDPad();
			break;
		case Input.Keys.SPACE:
			onButtonChange(0, ControllerButton.GREEN, ControllerButtonState.RELEASED);
			break;
		case Input.Keys.ENTER:
			onButtonChange(0, ControllerButton.YELLOW, ControllerButtonState.RELEASED);
			break;
		case Input.Keys.ESCAPE:
			onButtonChange(0, ControllerButton.RED, ControllerButtonState.RELEASED);
			break;
		case Input.Keys.TAB:
			onButtonChange(0, ControllerButton.BLUE, ControllerButtonState.RELEASED);
			break;
		case Input.Keys.F10:
			onButtonChange(0, ControllerButton.PAUSE, ControllerButtonState.RELEASED);
			break;
		case Input.Keys.PERIOD:
		case Input.Keys.X:
			onButtonChange(0, ControllerButton.RIGHT_SHOULDER, ControllerButtonState.RELEASED);
			break;
		case Input.Keys.COMMA:
		case Input.Keys.Z:
			onButtonChange(0, ControllerButton.LEFT_SHOULDER, ControllerButtonState.RELEASED);
			break;
		default:
			return false;
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public AvagadroBoard getBoard() {
		return board;
	}

	private void setBoard(AvagadroBoard board) {
		this.board = board;
	}
	
	public boolean canAbandonGame(){
		return getBoard()!=null;
	}
	
	public void abandonGame(){
		setBoard(null);
	}
	
	public boolean canResumeGame(){
		return getBoard()!=null;
	}
	
	public boolean canStartGame(){
		return getBoard()==null;
	}
	
	public void startGame(){
		setBoard(new AvagadroBoard(BOARD_COLUMNS,BOARD_ROWS,
				this.getTextureRegionManager(),
				BOARD_X,BOARD_Y,BOARD_CELL_WIDTH,BOARD_CELL_HEIGHT,
				BOARD_SLOTS,SLOTS_X,SLOTS_Y,SLOT_WIDTH,SLOT_HEIGHT,SLOTS_DELTA_X,SLOTS_DELTA_Y));
	}

	@Override
	public void onCompletion(Music music) {
		getMusicManager().play(getMusicGenerator().generate());
	}
}
