package com.playdeezgames.Avagadro.Board;

import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;
import com.playdeezgames.Avagadro.Enums.AvagadroTextures;
import com.playdeezgames.common.Board.BoardCellFactory;
import com.playdeezgames.common.Board.CardinalDirections;
import com.playdeezgames.common.Managers.TextureRegionManager;

public class AvagadroBoardCellFactory implements BoardCellFactory<CardinalDirections,AvagadroBoardCell> {
	private TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> regionManager;
	@Override
	public AvagadroBoardCell create() {
		return new AvagadroBoardCell(getRegionManager());
	}
	public TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> getRegionManager() {
		return regionManager;
	}
	private void setRegionManager(TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> regionManager) {
		this.regionManager = regionManager;
	}
	public AvagadroBoardCellFactory(TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> regionManager){
		setRegionManager(regionManager);
	}
}
