package com.playdeezgames.Avagadro.Board;

public enum Atoms {
	HELIUM(5,new CellValues[]{CellValues.HELIUM})
	,HYDROGEN(80,new CellValues[]{CellValues.HYDROGEN_N,CellValues.HYDROGEN_E,CellValues.HYDROGEN_S,CellValues.HYDROGEN_W})
	,OXYGEN(40,new CellValues[]{CellValues.OXYGEN_EW,CellValues.OXYGEN_NE,CellValues.OXYGEN_NS,CellValues.OXYGEN_NW,CellValues.OXYGEN_SE,CellValues.OXYGEN_SW})
	,NITROGEN(20,new CellValues[]{CellValues.NITROGEN_ESW,CellValues.NITROGEN_NES,CellValues.NITROGEN_SWN,CellValues.NITROGEN_WNE})
	,CARBON(10,new CellValues[]{CellValues.CARBON})
	;
	private int initialWeight;
	private CellValues[] cellValues;
	public CellValues[] getCellValues(){
		return cellValues;
	}
	public int getInitialWeight(){
		return initialWeight;
	}
	Atoms(int theInitialWeight,CellValues[] theCellValues){
		setInitialWeight(theInitialWeight);
		setCellValues(theCellValues);
	}
	private void setCellValues(CellValues[] theCellValues) {
		cellValues=theCellValues;
	}
	private void setInitialWeight(int theInitialWeight) {
		initialWeight=theInitialWeight;
	}
}
