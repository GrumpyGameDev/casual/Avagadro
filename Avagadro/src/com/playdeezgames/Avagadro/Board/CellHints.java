package com.playdeezgames.Avagadro.Board;

import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;

public enum CellHints {
	NONE(AvagadroTextureRegions.EMPTY_16X16),
	NO(AvagadroTextureRegions.HINT_NO),
	MAYBE(AvagadroTextureRegions.HINT_MAYBE),
	YES(AvagadroTextureRegions.HINT_YES);
	private AvagadroTextureRegions textureRegion;

	public AvagadroTextureRegions getTextureRegion() {
		return textureRegion;
	}

	private void setTextureRegion(AvagadroTextureRegions textureRegion) {
		this.textureRegion = textureRegion;
	}
	CellHints(AvagadroTextureRegions textureRegion){
		setTextureRegion(textureRegion);
	}
}
