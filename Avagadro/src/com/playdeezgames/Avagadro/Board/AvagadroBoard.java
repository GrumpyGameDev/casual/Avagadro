package com.playdeezgames.Avagadro.Board;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;
import com.playdeezgames.Avagadro.Enums.AvagadroTextures;
import com.playdeezgames.common.Board.Board;
import com.playdeezgames.common.Board.BoardColumn;
import com.playdeezgames.common.Board.CardinalDirections;
import com.playdeezgames.common.Generators.StandardWeightedGenerator;
import com.playdeezgames.common.Generators.WeightedGenerator;
import com.playdeezgames.common.Managers.TextureRegionManager;

public class AvagadroBoard extends Board<CardinalDirections,AvagadroBoardCell> {
	private AvagadroSlots slots;
	private AvagadroBoardCell currentCell;
	private Sprite cursorSprite;
	private Sprite ghostSprite;
	private WeightedGenerator<Atoms> atomicGenerator;
	private Map<Atoms,WeightedGenerator<CellValues>> cellValueGenerators = new HashMap<Atoms,WeightedGenerator<CellValues>>();
	private int score;
	public WeightedGenerator<Atoms> getAtomicGenerator(){
		if(atomicGenerator==null){
			atomicGenerator = new StandardWeightedGenerator<Atoms>(null);
			for(Atoms atom:Atoms.values()){
				atomicGenerator.setWeight(atom, atom.getInitialWeight());
			}
		}
		return atomicGenerator;
	}
	public WeightedGenerator<CellValues> getCellValueGenerator(Atoms atom){
		if(!cellValueGenerators.containsKey(atom)){
			WeightedGenerator<CellValues> generator = new StandardWeightedGenerator<CellValues>(null);
			for(CellValues cellValue : atom.getCellValues()){
				generator.setWeight(cellValue, cellValue.getWeight());
			}
			cellValueGenerators.put(atom,generator);
		}
		return cellValueGenerators.get(atom);
	}
	public CellValues generateNextCellValue(){
		Atoms atom = getAtomicGenerator().generate();
		return getCellValueGenerator(atom).generate();
	}
	public AvagadroBoard(
			int columns,
			int rows,
			TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> regionManager,
			float x,
			float y,
			float cellWidth,
			float cellHeight,
			int slots,
			float slotX,
			float slotY,
			float slotWidth,
			float slotHeight,
			float slotDeltaX,
			float slotDeltaY) {
		super(columns, rows, new AvagadroBoardCellFactory(regionManager), CardinalDirections.values());
		setSlots(new AvagadroSlots(this,slots,regionManager));
		setCursorSprite(regionManager.getNewSprite(AvagadroTextureRegions.CURSOR));
		getCursorSprite().setSize(cellWidth, cellHeight);
		setGhostSprite(regionManager.getNewSprite(AvagadroTextureRegions.EMPTY_16X16));
		getGhostSprite().setSize(cellWidth, cellHeight);
		getGhostSprite().setColor(1,1,1,0.25f);
		for(int column=0;column<size();++column){
			BoardColumn<CardinalDirections,AvagadroBoardCell> theColumn = this.get(column);
			for(int row=0;row<theColumn.size();++row){
				AvagadroBoardCell cell = theColumn.get(row);
				cell.setBoard(this);
				cell.setX(x+cellWidth*column);
				cell.setY(y+cellHeight*row);
				cell.setWidth(cellWidth);
				cell.setHeight(cellHeight);
			}
		}
		get(0).get(0).setCursor();
		getSlots().getCursorSprite().setSize(slotWidth, slotHeight);
		for(int slot=0;slot<getSlots().size();++slot){
			getSlots().get(slot).setX(slotX+slotDeltaX*slot);
			getSlots().get(slot).setY(slotY+slotDeltaY*slot);
			getSlots().get(slot).setWidth(slotWidth);
			getSlots().get(slot).setHeight(slotHeight);
			getSlots().get(slot).setValue(generateNextCellValue());
		}
	}

	public void draw(SpriteBatch batch){
		for(int column=0;column<size();++column){
			BoardColumn<CardinalDirections,AvagadroBoardCell> theColumn = this.get(column);
			for(int row=0;row<theColumn.size();++row){
				AvagadroBoardCell cell = theColumn.get(row);
				cell.draw(batch);
			}
		}
		getSlots().draw(batch);
	}

	public AvagadroBoardCell getCurrentCell() {
		return currentCell;
	}

	public void setCurrentCell(AvagadroBoardCell currentCell) {
		this.currentCell = currentCell;
	}

	public Sprite getCursorSprite() {
		return cursorSprite;
	}

	private void setCursorSprite(Sprite cursorSprite) {
		this.cursorSprite = cursorSprite;
	}

	public AvagadroSlots getSlots() {
		return slots;
	}

	private void setSlots(AvagadroSlots slots) {
		this.slots = slots;
	}
	public Sprite getGhostSprite() {
		return ghostSprite;
	}
	public void setGhostSprite(Sprite ghostSprite) {
		this.ghostSprite = ghostSprite;
	}
	public boolean placeAtom() {
		if(this.getCurrentCell().getHint()==CellHints.YES){
			getCurrentCell().putValue(getSlots().getCurrentSlot().getValue());
			getSlots().scrollSlots(generateNextCellValue());
			if(getCurrentCell().isMoleculeCompleted()){
				AvagadroMolecules molecule = getMarkedMolecule();
				if(molecule!=null){
					if(!getSlots().get(molecule.getSlotUnlocked()).isEnabled()){
						getSlots().get(molecule.getSlotUnlocked()).setEnabled(true);
					}
				}
				int markedScore = getMarkedScore();
				getCurrentCell().removeMolecule();
				setScore(getScore()+markedScore);
				if(markedScore<10){
					atomicGenerator.setWeight(Atoms.CARBON,atomicGenerator.getWeight(Atoms.CARBON)+2);
				}else if(markedScore<20){
					atomicGenerator.setWeight(Atoms.CARBON,atomicGenerator.getWeight(Atoms.CARBON)+1);
					atomicGenerator.setWeight(Atoms.NITROGEN,atomicGenerator.getWeight(Atoms.NITROGEN)+1);
				}else if(markedScore<50){
					atomicGenerator.setWeight(Atoms.NITROGEN,atomicGenerator.getWeight(Atoms.NITROGEN)+2);
				}else if(markedScore<100){
					atomicGenerator.setWeight(Atoms.NITROGEN,atomicGenerator.getWeight(Atoms.NITROGEN)+1);
					atomicGenerator.setWeight(Atoms.OXYGEN,atomicGenerator.getWeight(Atoms.OXYGEN)+1);
				}else if(markedScore<200){
					atomicGenerator.setWeight(Atoms.OXYGEN,atomicGenerator.getWeight(Atoms.OXYGEN)+2);
				}
				moveHeliums();
			}else{
				getCurrentCell().clearMarks();
			}
			return true;
		}else{
			return false;
		}
	}
	private AvagadroMolecules getMarkedMolecule() {
		int hydrogens = this.getMarkedAtomCount(Atoms.HYDROGEN);
		int oxygens = this.getMarkedAtomCount(Atoms.OXYGEN);
		int nitrogens = this.getMarkedAtomCount(Atoms.NITROGEN);
		int carbons = this.getMarkedAtomCount(Atoms.CARBON);
		for(AvagadroMolecules molecule:AvagadroMolecules.values()){
			if(molecule.getHydrogens()==hydrogens && molecule.getOxygens()==oxygens && molecule.getNitrogens()==nitrogens && molecule.getCarbons()==carbons){
				return molecule;
			}
		}
		return null;
	}
	private void setScore(int score) {
		this.score=score;
	}
	public int getScore() {
		return this.score;
	}
	private int getMarkedScore() {
		int total = 0;
		for(int column=0;column<this.size();++column){
			BoardColumn<CardinalDirections,AvagadroBoardCell> theColumn = this.get(column);
			for(int row=theColumn.size()-1;row>=0;--row){
				if(theColumn.get(row).isMarked()){
					total+=theColumn.get(row).getValue().getScore();
				}
			}
		}
		return total;
	}
	public void rotateSlotCW(){
		this.getSlots().getCurrentSlot().setValue(this.getSlots().getCurrentSlot().getValue().getCW());
	}
	public void moveHeliums(){
		for(int column=0;column<this.size();++column){
			BoardColumn<CardinalDirections,AvagadroBoardCell> theColumn = this.get(column);
			for(int row=theColumn.size()-1;row>=0;--row){
				AvagadroBoardCell theCell=theColumn.get(row);
				if(theCell.getValue()==CellValues.HELIUM){
					AvagadroBoardCell neighbor=theCell.getNeighbor(CardinalDirections.NORTH);
					if(neighbor==null){
						theCell.setFadeValue(CellValues.HELIUM);
						theCell.setFadeAlpha(1);
						theCell.putValue(CellValues.EMPTY);
						atomicGenerator.setWeight(Atoms.HELIUM,atomicGenerator.getWeight(Atoms.HELIUM)+1);
					}else if(neighbor.getValue().isEmpty()){
						if(neighbor.getHintForSpecificValue(CellValues.HELIUM)==CellHints.YES){
							theCell.putValue(CellValues.EMPTY);
							neighbor.putValue(CellValues.HELIUM);
							neighbor.setOffsetY(theCell.getY()-neighbor.getY());
							neighbor.setOffsetX(theCell.getX()-neighbor.getX());
						}
					}
				}
			}
		}
	}
	public int getMarkedAtomCount(Atoms atom){
		int total = 0;
		for(int column=0;column<this.size();++column){
			BoardColumn<CardinalDirections,AvagadroBoardCell> theColumn = this.get(column);
			for(int row=theColumn.size()-1;row>=0;--row){
				AvagadroBoardCell theCell=theColumn.get(row);
				if(theCell.isMarked() && theCell.getValue().getAtom()==atom){
					total++;
				}
			}
		}
		return total;
	}
	public boolean canPlayValue(CellValues theValue){
		for(int column=0;column<this.size();++column){
			BoardColumn<CardinalDirections,AvagadroBoardCell> theColumn = this.get(column);
			for(int row=theColumn.size()-1;row>=0;--row){
				AvagadroBoardCell theCell=theColumn.get(row);
				CellHints theHint = theCell.getHintForValue(theValue);
				if(theHint==CellHints.YES || theHint==CellHints.MAYBE){
					return true;
				}
			}
		}
		return false;
	}
	public boolean isGameOver(){
		for(int index=0;index<getSlots().size();++index){
			if(getSlots().get(index).isEnabled()){
				if(canPlayValue(getSlots().get(index).getValue())){
					return false;
				}
			}
		}
		return true;
	}
}
