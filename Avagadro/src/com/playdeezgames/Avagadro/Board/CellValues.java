package com.playdeezgames.Avagadro.Board;

import java.util.HashSet;
import java.util.Set;

import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;
import com.playdeezgames.common.Board.CardinalDirections;

public enum CellValues {
	EMPTY(true,false,false,false,false,0,AvagadroTextureRegions.EMPTY_16X16,0),
	HELIUM(false,false,false,false,false,0,AvagadroTextureRegions.HELIUM,1),
	HYDROGEN_N(false,true,false,false,false,1,AvagadroTextureRegions.HYDROGEN_N,1),
	HYDROGEN_E(false,false,true,false,false,1,AvagadroTextureRegions.HYDROGEN_E,1),
	HYDROGEN_S(false,false,false,true,false,1,AvagadroTextureRegions.HYDROGEN_S,1),
	HYDROGEN_W(false,false,false,false,true,1,AvagadroTextureRegions.HYDROGEN_W,1),
	OXYGEN_NS(false,true,false,true,false,2,AvagadroTextureRegions.OXYGEN_NS,1),
	OXYGEN_EW(false,false,true,false,true,2,AvagadroTextureRegions.OXYGEN_EW,1),
	OXYGEN_NE(false,true,true,false,false,2,AvagadroTextureRegions.OXYGEN_NE,1),
	OXYGEN_SE(false,false,true,true,false,2,AvagadroTextureRegions.OXYGEN_SE,1),
	OXYGEN_SW(false,false,false,true,true,2,AvagadroTextureRegions.OXYGEN_SW,1),
	OXYGEN_NW(false,true,false,false,true,2,AvagadroTextureRegions.OXYGEN_NW,1),
	NITROGEN_NES(false,true,true,true,false,3,AvagadroTextureRegions.NITROGEN_NES,1),
	NITROGEN_ESW(false,false,true,true,true,3,AvagadroTextureRegions.NITROGEN_ESW,1),
	NITROGEN_SWN(false,true,false,true,true,3,AvagadroTextureRegions.NITROGEN_SWN,1),
	NITROGEN_WNE(false,true,true,false,true,3,AvagadroTextureRegions.NITROGEN_WNE,1),
	CARBON(false,true,true,true,true,4,AvagadroTextureRegions.CARBON,1);
	private Set<CardinalDirections> links = new HashSet<CardinalDirections>();
	private boolean empty;
	private int score;
	private AvagadroTextureRegions textureRegion;
	private int weight;
	CellValues(boolean empty,boolean northLink,boolean eastLink,boolean southLink,boolean westLink,int score,AvagadroTextureRegions textureRegion,int weight){
		setTextureRegion(textureRegion);
		setWeight(weight);
		this.empty=empty;
		this.score=score;
		if(northLink){
			links.add(CardinalDirections.NORTH);
		}
		if(eastLink){
			links.add(CardinalDirections.EAST);
		}
		if(southLink){
			links.add(CardinalDirections.SOUTH);
		}
		if(westLink){
			links.add(CardinalDirections.WEST);
		}
	}
	private void setWeight(int weight) {
		this.weight=weight;
	}
	private void setTextureRegion(AvagadroTextureRegions textureRegion) {
		this.textureRegion=textureRegion;
	}
	public AvagadroTextureRegions getTextureRegion(){
		return this.textureRegion;
	}
	public boolean hasLink(CardinalDirections direction){
		return links.contains(direction);
	}
	public boolean isEmpty(){
		return empty;
	}
	public int getScore(){
		return score;
	}
	public int getWeight() {
		return weight;
	}
	public Atoms getAtom(){
		switch(this){
		case HELIUM:
			return Atoms.HELIUM;
		case HYDROGEN_N:
		case HYDROGEN_E:
		case HYDROGEN_S:
		case HYDROGEN_W:
			return Atoms.HYDROGEN;
		case OXYGEN_NS:
		case OXYGEN_EW:
		case OXYGEN_NE:
		case OXYGEN_SE:
		case OXYGEN_SW:
		case OXYGEN_NW:
			return Atoms.OXYGEN;
		case NITROGEN_NES:
		case NITROGEN_ESW:
		case NITROGEN_SWN:
		case NITROGEN_WNE:
			return Atoms.NITROGEN;
		case CARBON:
			return Atoms.CARBON;
		default:
			return null;
		}
	}
	public CellValues getCW(){
		switch(this){
		case HYDROGEN_N:
			return CellValues.HYDROGEN_E;
		case HYDROGEN_E:
			return CellValues.HYDROGEN_S;
		case HYDROGEN_S:
			return CellValues.HYDROGEN_W;
		case HYDROGEN_W:
			return CellValues.HYDROGEN_N;
		case OXYGEN_NS:
			return CellValues.OXYGEN_EW;
		case OXYGEN_EW:
			return CellValues.OXYGEN_NS;
		case OXYGEN_NE:
			return CellValues.OXYGEN_SE;
		case OXYGEN_SE:
			return CellValues.OXYGEN_SW;
		case OXYGEN_SW:
			return CellValues.OXYGEN_NW;
		case OXYGEN_NW:
			return CellValues.OXYGEN_NE;
		case NITROGEN_NES:
			return CellValues.NITROGEN_ESW;
		case NITROGEN_ESW:
			return CellValues.NITROGEN_SWN;
		case NITROGEN_SWN:
			return CellValues.NITROGEN_WNE;
		case NITROGEN_WNE:
			return CellValues.NITROGEN_NES;
		default:
			return this;
		}
	}
	public CellValues getCCW(){
		switch(this){
		case HYDROGEN_N:
			return CellValues.HYDROGEN_W;
		case HYDROGEN_E:
			return CellValues.HYDROGEN_N;
		case HYDROGEN_S:
			return CellValues.HYDROGEN_E;
		case HYDROGEN_W:
			return CellValues.HYDROGEN_S;
		case OXYGEN_NS:
			return CellValues.OXYGEN_EW;
		case OXYGEN_EW:
			return CellValues.OXYGEN_NS;
		case OXYGEN_NE:
			return CellValues.OXYGEN_NW;
		case OXYGEN_SE:
			return CellValues.OXYGEN_NE;
		case OXYGEN_SW:
			return CellValues.OXYGEN_SE;
		case OXYGEN_NW:
			return CellValues.OXYGEN_SW;
		case NITROGEN_NES:
			return CellValues.NITROGEN_WNE;
		case NITROGEN_ESW:
			return CellValues.NITROGEN_NES;
		case NITROGEN_SWN:
			return CellValues.NITROGEN_ESW;
		case NITROGEN_WNE:
			return CellValues.NITROGEN_SWN;
		default:
			return this;
		}
	}
	public CellValues[] getRotatedValues(){
		switch(this){
		case HYDROGEN_N:
			return new CellValues[]{CellValues.HYDROGEN_E,CellValues.HYDROGEN_S,CellValues.HYDROGEN_W};
		case HYDROGEN_E:
			return new CellValues[]{CellValues.HYDROGEN_S,CellValues.HYDROGEN_W,CellValues.HYDROGEN_N};
		case HYDROGEN_S:
			return new CellValues[]{CellValues.HYDROGEN_W,CellValues.HYDROGEN_N,CellValues.HYDROGEN_E};
		case HYDROGEN_W:
			return new CellValues[]{CellValues.HYDROGEN_N,CellValues.HYDROGEN_E,CellValues.HYDROGEN_S};
		case OXYGEN_NS:
			return new CellValues[]{CellValues.OXYGEN_EW};
		case OXYGEN_EW:
			return new CellValues[]{CellValues.OXYGEN_NS};
		case OXYGEN_NE:
			return new CellValues[]{CellValues.OXYGEN_SE,CellValues.OXYGEN_SW,CellValues.OXYGEN_NW};
		case OXYGEN_SE:
			return new CellValues[]{CellValues.OXYGEN_SW,CellValues.OXYGEN_NW,CellValues.OXYGEN_NE};
		case OXYGEN_SW:
			return new CellValues[]{CellValues.OXYGEN_NW,CellValues.OXYGEN_NE,CellValues.OXYGEN_SE};
		case OXYGEN_NW:
			return new CellValues[]{CellValues.OXYGEN_NE,CellValues.OXYGEN_SE,CellValues.OXYGEN_SW};
		case NITROGEN_NES:
			return new CellValues[]{CellValues.NITROGEN_ESW,CellValues.NITROGEN_SWN,CellValues.NITROGEN_WNE};
		case NITROGEN_ESW:
			return new CellValues[]{CellValues.NITROGEN_SWN,CellValues.NITROGEN_WNE,CellValues.NITROGEN_NES};
		case NITROGEN_SWN:
			return new CellValues[]{CellValues.NITROGEN_WNE,CellValues.NITROGEN_NES,CellValues.NITROGEN_ESW};
		case NITROGEN_WNE:
			return new CellValues[]{CellValues.NITROGEN_NES,CellValues.NITROGEN_ESW,CellValues.NITROGEN_SWN};
		default:
			return new CellValues[0];
		}
	}
}
