package com.playdeezgames.Avagadro.Board;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;
import com.playdeezgames.Avagadro.Enums.AvagadroTextures;
import com.playdeezgames.common.Managers.TextureRegionManager;

public class AvagadroSlots {
	private AvagadroBoard board;
	private List<AvagadroSlot> slots = new ArrayList<AvagadroSlot>();
	private AvagadroSlot currentSlot=null;
	private Sprite cursorSprite = null;
	public int size(){
		return slots.size();
	}
	public AvagadroSlot get(int slot){
		return slots.get(slot);
	}
	public AvagadroSlots(AvagadroBoard board,int size, TextureRegionManager<AvagadroTextures, AvagadroTextureRegions> regionManager){
		setCursorSprite(regionManager.getNewSprite(AvagadroTextureRegions.CURSOR));
		setBoard(board);
		AvagadroSlot previous=null;
		AvagadroSlot current=null;
		while(size()<size){
			previous=current;
			current = new AvagadroSlot(this,regionManager);
			current.setPreviousSlot(previous);
			if(previous!=null){
				previous.setNextSlot(current);
			}
			slots.add(current);
		}
		get(size()-1).setNextSlot(get(0));
		get(0).setPreviousSlot(get(size()-1));
		setCurrentSlot(get(0));
		getCurrentSlot().setEnabled(true);
	}
	public AvagadroSlot getCurrentSlot() {
		return currentSlot;
	}
	public void setCurrentSlot(AvagadroSlot currentSlot) {
		this.currentSlot = currentSlot;
	}
	public AvagadroBoard getBoard() {
		return board;
	}
	private void setBoard(AvagadroBoard board) {
		this.board = board;
	}
	public Sprite getCursorSprite() {
		return cursorSprite;
	}
	private void setCursorSprite(Sprite cursorSprite) {
		this.cursorSprite = cursorSprite;
	}
	public void draw(SpriteBatch batch) {
		for(AvagadroSlot slot: slots){
			slot.draw(batch);
		}
	}
	public void scrollSlots(CellValues nextValue){
		AvagadroSlot theSlot = getCurrentSlot();
		while(theSlot.getNextSlot()!=get(0)){
			theSlot.setValue(theSlot.getNextSlot().getValue());
			theSlot=theSlot.getNextSlot();
		}
		theSlot.setValue(nextValue);
	}
	public void selectNextSlot(){
		do{
			setCurrentSlot(getCurrentSlot().getNextSlot());
		}while(!getCurrentSlot().isEnabled());
	}
	public void selectPreviousSlot() {
		do{
			setCurrentSlot(getCurrentSlot().getPreviousSlot());
		}while(!getCurrentSlot().isEnabled());
	}
}
