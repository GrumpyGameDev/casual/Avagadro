package com.playdeezgames.Avagadro.Board;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;
import com.playdeezgames.Avagadro.Enums.AvagadroTextures;
import com.playdeezgames.common.Board.BoardCell;
import com.playdeezgames.common.Board.CardinalDirections;
import com.playdeezgames.common.Managers.TextureRegionManager;

public class AvagadroBoardCell extends BoardCell<CardinalDirections,AvagadroBoardCell> {
	private TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> regionManager;
	private CellValues value;
	private Sprite sprite;
	private Sprite hintSprite;
	private Sprite fadeSprite;
	private CellValues fadeValue;
	private AvagadroBoard board;
	private boolean marked;
	private float x;
	private float y;
	private float offsetX=0;
	private float offsetY=0;
	private float fadeAlpha=0;
	public CellValues getValue(){
		return value;
	}
	public CellHints getHintForSpecificValue(CellValues theValue){
		for(CardinalDirections direction: CardinalDirections.values()){
			AvagadroBoardCell theNeighbor = this.getNeighbor(direction);
			if(theValue.hasLink(direction)){
				if(theNeighbor==null){
					return CellHints.NO;
				}else{
					if(!theNeighbor.getValue().isEmpty() && !theNeighbor.getValue().hasLink(direction.opposite())){
						return CellHints.NO;
					}
				}
			}else{
				if(theNeighbor!=null){
					if(!theNeighbor.getValue().isEmpty() && theNeighbor.getValue().hasLink(direction.opposite())){
						return CellHints.NO;
					}
				}
			}
		}
		return CellHints.YES;
	}
	public CellHints getHintForValue(CellValues theValue){
		if(!getValue().isEmpty()){
			return CellHints.NONE;
		}else{
			if(getHintForSpecificValue(theValue)==CellHints.YES){
				return CellHints.YES;
			}else{
				for(CellValues otherValue:theValue.getRotatedValues()){
					if(getHintForSpecificValue(otherValue)==CellHints.YES){
						return CellHints.MAYBE;
					}
				}
				return CellHints.NO;
			}
		}
	}
	public CellHints getHint(){
		return getHintForValue(getBoard().getSlots().getCurrentSlot().getValue());
	}
	public void putValue(CellValues value){
		this.value=value;
		getSprite().setRegion(getRegionManager().getTextureRegion(value.getTextureRegion()));
	}
	public AvagadroBoardCell(TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> regionManager){
		setRegionManager(regionManager);
		setBoard(board);
		putValue(CellValues.EMPTY);
		setFadeValue(CellValues.EMPTY);
	}
	public void draw(SpriteBatch batch) {
		getHintSprite().setRegion(getRegionManager().getTextureRegion(getHint().getTextureRegion()));
		getHintSprite().draw(batch);
		getSprite().draw(batch);
		if(getFadeAlpha()>0){
			getFadeSprite().draw(batch);
			float alpha=getFadeAlpha()-0.04f;
			if(alpha<0) alpha=0;
			setFadeAlpha(alpha);
		}
		if(isCursor()){
			getBoard().getCursorSprite().setPosition(getX(), getY());
			getBoard().getCursorSprite().draw(batch);
			getBoard().getGhostSprite().setRegion(getRegionManager().getTextureRegion(getBoard().getSlots().getCurrentSlot().getValue().getTextureRegion()));
			getBoard().getGhostSprite().setPosition(getX(), getY());
			getBoard().getGhostSprite().draw(batch);
		}
		if(getOffsetX()>0){
			setOffsetX(getOffsetX()-1);
		}else if(getOffsetX()<0){
			setOffsetX(getOffsetX()+1);
		}
		if(getOffsetY()>0){
			setOffsetY(getOffsetY()-1);
		}else if(getOffsetY()<0){
			setOffsetY(getOffsetY()+1);
		}
	}
	private void updateFadeSpritePosition(){
		getFadeSprite().setX(getX());
		getFadeSprite().setY(getY());
	}
	private void updateSpritePosition(){
		getSprite().setX(getX()+getOffsetX());
		getSprite().setY(getY()+getOffsetY());
	}
	private void updateHintSpritePosition(){
		getHintSprite().setX(getX());
		getHintSprite().setY(getY());
	}
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x=x;
		updateSpritePosition();
		updateHintSpritePosition();
		updateFadeSpritePosition();
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y=y;
		updateSpritePosition();
		updateHintSpritePosition();
		updateFadeSpritePosition();
	}
	public float getWidth() {
		return getSprite().getWidth();
	}
	public void setWidth(float width) {
		getSprite().setSize(width, getHeight());
		getHintSprite().setSize(width, getHeight());
		getFadeSprite().setSize(width, getHeight());
	}
	public float getHeight() {
		return getSprite().getHeight();
	}
	public void setHeight(float height) {
		getSprite().setSize(getWidth(), height);
		getHintSprite().setSize(getWidth(), height);
		getFadeSprite().setSize(getWidth(), height);
	}
	public Sprite getSprite() {
		if(sprite==null){
			setSprite(new Sprite());
		}
		return sprite;
	}
	public Sprite getFadeSprite(){
		if(fadeSprite==null){
			setFadeSprite(new Sprite());
		}
		return fadeSprite;
	}
	private void setFadeSprite(Sprite sprite2) {
		fadeSprite=sprite2;
	}
	private void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}
	public TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> getRegionManager() {
		return regionManager;
	}
	public void setRegionManager(TextureRegionManager<AvagadroTextures,AvagadroTextureRegions> regionManager) {
		this.regionManager = regionManager;
	}
	public AvagadroBoard getBoard() {
		return board;
	}
	public void setBoard(AvagadroBoard board) {
		this.board = board;
	}
	public boolean isCursor(){
		return getBoard().getCurrentCell()==this;
	}
	public void setCursor(){
		getBoard().setCurrentCell(this);
	}
	public Sprite getHintSprite() {
		if(this.hintSprite==null){
			setHintSprite(new Sprite());
		}
		return hintSprite;
	}
	private void setHintSprite(Sprite hintSprite) {
		this.hintSprite = hintSprite;
	}
	public boolean isMoleculeCompleted() {
		CellValues theValue = getValue();
		setMarked(true);
		if(theValue==CellValues.HELIUM){
			//helium is different
			return false;
		}else{
			for(CardinalDirections direction:CardinalDirections.values()){
				if(theValue.hasLink(direction)){
					AvagadroBoardCell neighbor = getNeighbor(direction);
					if(!neighbor.isMarked()){
						if(neighbor.getValue().isEmpty()){
							return false;
						}else{
							if(!neighbor.isMoleculeCompleted()){
								return false;
							}
						}
					}
				}
			}
			return true;
		}
	}
	public void removeMolecule() {
		CellValues theValue = getValue();
		setFadeValue(getValue());
		setFadeAlpha(1);
		putValue(CellValues.EMPTY);
		setMarked(false);
		for(CardinalDirections direction:CardinalDirections.values()){
			if(theValue.hasLink(direction)){
				AvagadroBoardCell neighbor = getNeighbor(direction);
				if(!neighbor.getValue().isEmpty()){
					neighbor.removeMolecule();
				}
			}
		}
	}
	public boolean isMarked() {
		return marked;
	}
	public void setMarked(boolean marked) {
		this.marked = marked;
	}
	public void clearMarks() {
		setMarked(false);
		CellValues theValue = getValue();
		for(CardinalDirections direction:CardinalDirections.values()){
			if(theValue.hasLink(direction)){
				AvagadroBoardCell neighbor = getNeighbor(direction);
				if(neighbor.isMarked()){
					neighbor.clearMarks();
				}
			}
		}
	}
	public float getOffsetX() {
		return offsetX;
	}
	public void setOffsetX(float offsetX) {
		this.offsetX = offsetX;
		updateSpritePosition();
	}
	public float getOffsetY() {
		return offsetY;
	}
	public void setOffsetY(float offsetY) {
		this.offsetY = offsetY;
		updateSpritePosition();
	}
	public CellValues getFadeValue() {
		return fadeValue;
	}
	public void setFadeValue(CellValues fadeValue) {
		this.fadeValue = fadeValue;
		getFadeSprite().setRegion(getRegionManager().getTextureRegion(fadeValue.getTextureRegion()));
	}
	public float getFadeAlpha() {
		return fadeAlpha;
	}
	public void setFadeAlpha(float fadeAlpha) {
		this.fadeAlpha = fadeAlpha;
		getFadeSprite().setColor(1,1,1,fadeAlpha);
	}
}
