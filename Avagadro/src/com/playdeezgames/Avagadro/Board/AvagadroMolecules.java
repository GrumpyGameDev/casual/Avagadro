package com.playdeezgames.Avagadro.Board;

public enum AvagadroMolecules {
	MOLECULAR_HYDROGEN(2,0,0,0,0),
	WATER(2,1,0,0,0),
	HYDROGEN_PEROXIDE(2,2,0,0,0),
	AMMONIA(3,0,1,0,0),
	METHANE(4,0,0,1,1),
	ETHANE(6,0,0,2,2),
	PROPANE(8,0,0,3,3),
	BUTANE(10,0,0,4,4),
	PENTANE(12,0,0,5,5),
	HEXANE(14,0,0,6,6),
	HEPTANE(16,0,0,7,7),
	METHANOL(4,1,0,1,1),
	ETHANOL(6,1,0,2,2),
	PROPANOL(8,1,0,3,3),
	BUTANOL(10,1,0,4,4),
	PENTANOL(12,1,0,5,5),
	HEXANOL(14,1,0,6,6),
	HEPTANOL(16,1,0,7,7);
	int hydrogens;
	int oxygens;
	int nitrogens;
	int carbons;
	int slotUnlocked;
	AvagadroMolecules(int hydrogens,int oxygens,int nitrogens,int carbons,int slotUnlocked){
		this.hydrogens=hydrogens;
		this.oxygens=oxygens;
		this.nitrogens=nitrogens;
		this.carbons=carbons;
		this.slotUnlocked=slotUnlocked;
	}
	int getHydrogens(){
		return hydrogens;
	}
	int getOxygens(){
		return oxygens;
	}
	int getNitrogens(){
		return nitrogens;
	}
	int getCarbons(){
		return carbons;
	}
	int getSlotUnlocked(){
		return slotUnlocked;
	}
}
