package com.playdeezgames.Avagadro.Board;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;
import com.playdeezgames.Avagadro.Enums.AvagadroTextures;
import com.playdeezgames.common.Managers.TextureRegionManager;

public class AvagadroSlot {
	private AvagadroSlot nextSlot;
	private AvagadroSlot previousSlot;
	private AvagadroSlots slots;
	private boolean enabled=false;
	private TextureRegionManager<AvagadroTextures, AvagadroTextureRegions> regionManager;
	private Sprite sprite;
	private CellValues value;
	public AvagadroSlot(AvagadroSlots avagadroSlots, TextureRegionManager<AvagadroTextures, AvagadroTextureRegions> regionManager) {
		setRegionManager(regionManager);
		setSlots(avagadroSlots);
		setValue(CellValues.HELIUM);
	}
	private void setSlots(AvagadroSlots avagadroSlots) {
		slots=avagadroSlots;
	}
	public AvagadroSlots getSlots(){
		return slots;
	}
	public AvagadroSlot getNextSlot() {
		return nextSlot;
	}
	public void setNextSlot(AvagadroSlot nextSlot) {
		this.nextSlot = nextSlot;
	}
	public AvagadroSlot getPreviousSlot() {
		return previousSlot;
	}
	public void setPreviousSlot(AvagadroSlot previousSlot) {
		this.previousSlot = previousSlot;
	}
	public void setEnabled(boolean enabled) {
		this.enabled=enabled;
	}
	public boolean isEnabled(){
		return this.enabled;
	}
	public Sprite getSprite() {
		if(sprite==null){
			setSprite(new Sprite());
		}
		return sprite;
	}
	private void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}
	public void setX(float x) {
		getSprite().setX(x);
	}
	public void setY(float y) {
		getSprite().setY(y);
	}
	public void setWidth(float cellWidth) {
		getSprite().setSize(cellWidth, getHeight());
	}
	private float getHeight() {
		return getSprite().getHeight();
	}
	public void setHeight(float cellHeight) {
		getSprite().setSize(getWidth(), cellHeight);
	}
	private float getWidth() {
		return getSprite().getWidth();
	}
	public TextureRegionManager<AvagadroTextures, AvagadroTextureRegions> getRegionManager() {
		return regionManager;
	}
	private void setRegionManager(TextureRegionManager<AvagadroTextures, AvagadroTextureRegions> regionManager) {
		this.regionManager = regionManager;
	}
	public CellValues getValue() {
		return value;
	}
	public void setValue(CellValues value) {
		this.value = value;
		getSprite().setRegion(getRegionManager().getTextureRegion(value.getTextureRegion()));
	}
	public void draw(SpriteBatch batch){
		if(isEnabled()){
			getSprite().setColor(1, 1, 1, 1);
		}else{
			getSprite().setColor(1, 1, 1, 0.0f);
		}
		getSprite().draw(batch);
		if(isCurrentSlot()){
			getSlots().getCursorSprite().setPosition(getX(),getY());
			getSlots().getCursorSprite().draw(batch);
		}
	}
	private float getY() {
		return getSprite().getY();
	}
	private float getX() {
		return getSprite().getX();
	}
	private boolean isCurrentSlot() {
		return getSlots().getCurrentSlot()==this;
	}
}
