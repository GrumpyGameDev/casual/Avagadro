package com.playdeezgames.Avagadro.Enums;

import com.playdeezgames.common.Managers.TextureRegionDescriptor;

public enum AvagadroTextureRegions implements TextureRegionDescriptor<AvagadroTextures>{
	SPLASH(AvagadroTextures.SPLASH,0,0,1024,512)
	,GREEN_BUTTON(AvagadroTextures.GREEN_BUTTON,0,0,128,128)
	,BLUE_BUTTON(AvagadroTextures.BLUE_BUTTON,0,0,128,128)
	,YELLOW_BUTTON(AvagadroTextures.YELLOW_BUTTON,0,0,128,128)
	,RED_BUTTON(AvagadroTextures.RED_BUTTON,0,0,128,128)
	,EMPTY_16X16(AvagadroTextures.EMPTY_16X16,0,0,16,16)
	,HELIUM(AvagadroTextures.HELIUM,10,10,108,108)
	,HYDROGEN_N(AvagadroTextures.HYDROGEN_N,10,10,108,108)
	,HYDROGEN_E(AvagadroTextures.HYDROGEN_E,10,10,108,108)
	,HYDROGEN_S(AvagadroTextures.HYDROGEN_S,10,10,108,108)
	,HYDROGEN_W(AvagadroTextures.HYDROGEN_W,10,10,108,108)
	,OXYGEN_NS(AvagadroTextures.OXYGEN_NS,10,10,108,108)
	,OXYGEN_EW(AvagadroTextures.OXYGEN_EW,10,10,108,108)
	,OXYGEN_NE(AvagadroTextures.OXYGEN_NE,10,10,108,108)
	,OXYGEN_SE(AvagadroTextures.OXYGEN_SE,10,10,108,108)
	,OXYGEN_SW(AvagadroTextures.OXYGEN_SW,10,10,108,108)
	,OXYGEN_NW(AvagadroTextures.OXYGEN_NW,10,10,108,108)
	,NITROGEN_NES(AvagadroTextures.NITROGEN_NES,10,10,108,108)
	,NITROGEN_ESW(AvagadroTextures.NITROGEN_ESW,10,10,108,108)
	,NITROGEN_SWN(AvagadroTextures.NITROGEN_SWN,10,10,108,108)
	,NITROGEN_WNE(AvagadroTextures.NITROGEN_WNE,10,10,108,108)
	,CARBON(AvagadroTextures.CARBON,10,10,108,108)
	,CURSOR(AvagadroTextures.CURSOR,10,10,108,108)
	,HINT_NO(AvagadroTextures.HINT_NO,10,10,108,108)
	,HINT_YES(AvagadroTextures.HINT_YES,10,10,108,108)
	,HINT_MAYBE(AvagadroTextures.HINT_MAYBE,10,10,108,108)
	;

	AvagadroTextures textureDescriptor;
	int x;
	int y;
	int width;
	int height;
	@Override
	public AvagadroTextures getTextureDescriptor() {
		return textureDescriptor;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	AvagadroTextureRegions(AvagadroTextures theTextureDescriptor,int theX,int theY,int theWidth,int theHeight){
		textureDescriptor = theTextureDescriptor;
		x = theX;
		y = theY;
		width = theWidth;
		height = theHeight;
	}
}
