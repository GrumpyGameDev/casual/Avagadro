package com.playdeezgames.Avagadro.Enums;

import com.playdeezgames.common.Managers.MusicDescriptor;

public enum AvagadroMusic implements MusicDescriptor {
	PROLOGUE("data/audio/music/Kai_Engel_-_01_-_Prologue.mp3",1),
	CALLS_AND_ECHOES("data/audio/music/Kai_Engel_-_02_-_Calls_and_Echoes.mp3",1),
	FAIRYTALE("data/audio/music/Kai_Engel_-_03_-_Fairytale.mp3",1),
	RAINING("data/audio/music/Kai_Engel_-_04_-_Raining.mp3",1),
	VINTAGE_FRAMES("data/audio/music/Kai_Engel_-_05_-_Vintage_Frames.mp3",1),
	THE_FLAMES_OF_ROME("data/audio/music/Kai_Engel_-_06_-_The_Flames_of_Rome.mp3",1),
	PHANTASM("data/audio/music/Kai_Engel_-_07_-_Phantasm.mp3",1),
	WHEN_THE_LIGHTS_CAME_ON("data/audio/music/Kai_Engel_-_08_-_When_the_Lights_Came_On.mp3",1),
	EMBRACING_THE_SUNRISE("data/audio/music/Kai_Engel_-_09_-_Embracing_The_Sunrise.mp3",1);

	private String fileName;
	private int weight;
	@Override
	public String getFileName() {
		return fileName;
	}
	public int getWeight(){
		return this.weight;
	}
	AvagadroMusic(String fileName,int weight){
		this.fileName=fileName;
		this.weight=weight;
	}
}
