package com.playdeezgames.Avagadro.Enums;

public enum AvagadroStates {
	SPLASH,
	MAIN_MENU,
	CONFIRM_QUIT,
	HOW_TO_PLAY,
	ABOUT,
	GOOD_BYE, OPTIONS, PLAY, CONFIRM_ABANDON;
}
