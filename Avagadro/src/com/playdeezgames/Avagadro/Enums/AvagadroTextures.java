package com.playdeezgames.Avagadro.Enums;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.playdeezgames.common.Managers.TextureDescriptor;

public enum AvagadroTextures implements TextureDescriptor{
	SPLASH("data/images/logo.png",TextureFilter.Linear,TextureFilter.Linear)
	,GREEN_BUTTON("data/images/ouya_buttons/OUYA_O.png",TextureFilter.Linear,TextureFilter.Linear)
	,RED_BUTTON("data/images/ouya_buttons/OUYA_A.png",TextureFilter.Linear,TextureFilter.Linear)
	,YELLOW_BUTTON("data/images/ouya_buttons/OUYA_Y.png",TextureFilter.Linear,TextureFilter.Linear)
	,BLUE_BUTTON("data/images/ouya_buttons/OUYA_U.png",TextureFilter.Linear,TextureFilter.Linear)
	,EMPTY_16X16("data/images/empty16x16.png",TextureFilter.Linear,TextureFilter.Linear)
	,HELIUM("data/images/atoms/0.png",TextureFilter.Linear,TextureFilter.Linear)
	,HYDROGEN_N("data/images/atoms/1.png",TextureFilter.Linear,TextureFilter.Linear)
	,HYDROGEN_E("data/images/atoms/2.png",TextureFilter.Linear,TextureFilter.Linear)
	,HYDROGEN_S("data/images/atoms/4.png",TextureFilter.Linear,TextureFilter.Linear)
	,HYDROGEN_W("data/images/atoms/8.png",TextureFilter.Linear,TextureFilter.Linear)
	,OXYGEN_NS("data/images/atoms/5.png",TextureFilter.Linear,TextureFilter.Linear)
	,OXYGEN_EW("data/images/atoms/10.png",TextureFilter.Linear,TextureFilter.Linear)
	,OXYGEN_NE("data/images/atoms/3.png",TextureFilter.Linear,TextureFilter.Linear)
	,OXYGEN_SE("data/images/atoms/6.png",TextureFilter.Linear,TextureFilter.Linear)
	,OXYGEN_SW("data/images/atoms/12.png",TextureFilter.Linear,TextureFilter.Linear)
	,OXYGEN_NW("data/images/atoms/9.png",TextureFilter.Linear,TextureFilter.Linear)
	,NITROGEN_NES("data/images/atoms/7.png",TextureFilter.Linear,TextureFilter.Linear)
	,NITROGEN_ESW("data/images/atoms/14.png",TextureFilter.Linear,TextureFilter.Linear)
	,NITROGEN_SWN("data/images/atoms/13.png",TextureFilter.Linear,TextureFilter.Linear)
	,NITROGEN_WNE("data/images/atoms/11.png",TextureFilter.Linear,TextureFilter.Linear)
	,CARBON("data/images/atoms/15.png",TextureFilter.Linear,TextureFilter.Linear)
	,CURSOR("data/images/atoms/cursor.png",TextureFilter.Linear,TextureFilter.Linear)
	,HINT_NO("data/images/atoms/no.png",TextureFilter.Linear,TextureFilter.Linear)
	,HINT_YES("data/images/atoms/yes.png",TextureFilter.Linear,TextureFilter.Linear)
	,HINT_MAYBE("data/images/atoms/maybe.png",TextureFilter.Linear,TextureFilter.Linear)
	;

	private String fileName;
	private TextureFilter minFilter;
	private TextureFilter magFilter;
	@Override
	public String getFileName() {
		return fileName;
	}

	@Override
	public TextureFilter getMinFilter() {
		return minFilter;
	}

	@Override
	public TextureFilter getMagFilter() {
		return magFilter;
	}
	AvagadroTextures(String theFileName,TextureFilter theMinFilter,TextureFilter theMagFilter){
		fileName=theFileName;
		minFilter = theMinFilter;
		magFilter = theMagFilter;
	}
}
