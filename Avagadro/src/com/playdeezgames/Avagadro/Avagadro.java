package com.playdeezgames.Avagadro;

import com.badlogic.gdx.ApplicationListener;
import com.playdeezgames.common.Controller.AbstractControllerFactory;

public class Avagadro implements ApplicationListener {
	
	public Avagadro(AbstractControllerFactory theAbstractControllerFactory){
		machine = new AvagadroStateMachine(theAbstractControllerFactory);
	}
	
	private AvagadroStateMachine machine;
	
	@Override
	public void create() {	
		machine.create();
	}

	@Override
	public void dispose() {
		machine.dispose();
	}

	@Override
	public void render() {
		machine.render();
	}

	@Override
	public void resize(int width, int height) {
		machine.resize(width, height);
	}

	@Override
	public void pause() {
		machine.pause();
	}

	@Override
	public void resume() {
		machine.resume();
	}
}
