package com.playdeezgames.Avagadro.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.playdeezgames.Avagadro.AvagadroStateMachine;
import com.playdeezgames.Avagadro.Enums.AvagadroStates;
import com.playdeezgames.common.Ascii.AsciiCharacters;
import com.playdeezgames.common.Ascii.AsciiColors;
import com.playdeezgames.common.Controller.ControllerAxis;
import com.playdeezgames.common.Controller.ControllerButton;
import com.playdeezgames.common.Controller.ControllerButtonState;
import com.playdeezgames.common.Controller.ControllerDPadState;
import com.playdeezgames.common.StateMachine.State;

public class OptionsState extends BaseState implements State{

	public OptionsState(AvagadroStateMachine theMachine) {
		super(theMachine);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onAxisChange(int controller, ControllerAxis axis, float value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onButtonChange(int controller, ControllerButton button,
			ControllerButtonState state) {
		if(state==ControllerButtonState.PRESSED){
			if(button==ControllerButton.RED){
				getMachine().setCurrent(AvagadroStates.MAIN_MENU);
			}
		}
	}

	@Override
	public void onDPadChange(int controller, ControllerDPadState state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		getMachine().getSpriteBatch().setProjectionMatrix(getMachine().getCamera().combined);
		getMachine().getSpriteBatch().begin();
		getMachine().getAsciiTextGrid().draw(getMachine().getSpriteBatch());
		getMachine().getSpriteBatch().end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start() {
		getMachine().getAsciiTextGrid().fill(AsciiCharacters.CHARACTER_0,AsciiColors.TRANSPARENT,AsciiColors.TRANSPARENT);		
		getMachine().getAsciiTextGrid().WriteText(0, getMachine().getAsciiTextGrid().getHeight()-1, "Options", AsciiColors.BLACK, AsciiColors.TRANSPARENT);
	}

}
