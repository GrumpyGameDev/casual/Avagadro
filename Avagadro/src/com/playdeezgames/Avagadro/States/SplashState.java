package com.playdeezgames.Avagadro.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.TimeUtils;
import com.playdeezgames.Avagadro.AvagadroStateMachine;
import com.playdeezgames.Avagadro.Enums.AvagadroStates;
import com.playdeezgames.Avagadro.Enums.AvagadroTextureRegions;
import com.playdeezgames.common.Controller.ControllerAxis;
import com.playdeezgames.common.Controller.ControllerButton;
import com.playdeezgames.common.Controller.ControllerButtonState;
import com.playdeezgames.common.Controller.ControllerDPadState;
import com.playdeezgames.common.StateMachine.State;

public class SplashState extends BaseState implements State{
	private static final long SPLASH_TIMER = 2000;
	private long startTime;
	private Sprite logoSprite;
	
	public SplashState(AvagadroStateMachine theMachine) {
		super(theMachine);
	}

	@Override
	public void dispose() {
	}

	@Override
	public void render() {
		long currentTime = TimeUtils.millis()-startTime;
		getLogoSprite().setColor(1,1,1,getAlpha((float)currentTime/(float)SPLASH_TIMER));
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		getMachine().getSpriteBatch().setProjectionMatrix(getMachine().getCamera().combined);
		getMachine().getSpriteBatch().begin();
		getLogoSprite().draw(getMachine().getSpriteBatch());
		getMachine().getSpriteBatch().end();
		if(currentTime>SPLASH_TIMER){
			getMachine().setCurrent(AvagadroStates.MAIN_MENU);
		}
		
	}

	private float getAlpha(float progress) {
		if(progress<0.25f){
			return progress*4;
		}else if(progress>=0.25f && progress<0.75f){
			return 1;
		}else if(progress>=0.75f && progress<1){
			return 1-(progress-0.75f)*4;
		}else{
			return 0;
		}
	}

	private Sprite getLogoSprite() {
		if(logoSprite==null){
			logoSprite = getMachine().getTextureRegionManager().getNewSprite(AvagadroTextureRegions.SPLASH);
			logoSprite.setColor(1, 1, 1, 1);
			logoSprite.setSize(AvagadroTextureRegions.SPLASH.getWidth(),AvagadroTextureRegions.SPLASH.getHeight());
			logoSprite.setPosition(-AvagadroTextureRegions.SPLASH.getWidth()/2,-AvagadroTextureRegions.SPLASH.getHeight()/2);
		}
		return  logoSprite;
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void create() {
	}

	@Override
	public void finish() {
	}

	@Override
	public void start() {
		startTime = TimeUtils.millis();
	}

	@Override
	public void onAxisChange(int controller, ControllerAxis axis, float value) {
	}

	@Override
	public void onButtonChange(int controller, ControllerButton button,
			ControllerButtonState state) {
		if(state==ControllerButtonState.PRESSED){
			getMachine().setCurrent(AvagadroStates.MAIN_MENU);
		}
	}

	@Override
	public void onDPadChange(int controller, ControllerDPadState state) {
	}

}
