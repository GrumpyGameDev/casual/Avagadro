package com.playdeezgames.Avagadro.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.playdeezgames.Avagadro.AvagadroStateMachine;
import com.playdeezgames.Avagadro.Board.AvagadroBoardCell;
import com.playdeezgames.Avagadro.Enums.AvagadroStates;
import com.playdeezgames.common.Ascii.AsciiCharacters;
import com.playdeezgames.common.Ascii.AsciiColors;
import com.playdeezgames.common.Board.CardinalDirections;
import com.playdeezgames.common.Controller.ControllerAxis;
import com.playdeezgames.common.Controller.ControllerButton;
import com.playdeezgames.common.Controller.ControllerButtonState;
import com.playdeezgames.common.Controller.ControllerDPadState;
import com.playdeezgames.common.StateMachine.State;

public class PlayState extends BaseState implements State{

	public PlayState(AvagadroStateMachine theMachine) {
		super(theMachine);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onAxisChange(int controller, ControllerAxis axis, float value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onButtonChange(int controller, ControllerButton button,
			ControllerButtonState state) {
		if(state==ControllerButtonState.PRESSED){
			if(button==ControllerButton.PAUSE){
				getMachine().setCurrent(AvagadroStates.MAIN_MENU);
			}else if(button==ControllerButton.GREEN){
				getMachine().getBoard().placeAtom();
			}else if(button==ControllerButton.YELLOW){
				getMachine().getBoard().getSlots().selectNextSlot();
			}else if(button==ControllerButton.BLUE){
				getMachine().getBoard().getSlots().selectPreviousSlot();
			}else if(button==ControllerButton.RIGHT_SHOULDER){
				getMachine().getBoard().rotateSlotCW();
			}else if(button==ControllerButton.LEFT_SHOULDER){
				getMachine().getBoard().rotateSlotCW();
				getMachine().getBoard().rotateSlotCW();
				getMachine().getBoard().rotateSlotCW();
			}
		}
	}

	@Override
	public void onDPadChange(int controller, ControllerDPadState state) {
		switch(state){
		case EAST:
			moveCursor(CardinalDirections.EAST);
			break;
		case NORTH:
			moveCursor(CardinalDirections.NORTH);
			break;
		case SOUTH:
			moveCursor(CardinalDirections.SOUTH);
			break;
		case WEST:
			moveCursor(CardinalDirections.WEST);
			break;
		default:
			break;
		}
	}

	private void moveCursor(CardinalDirections theDirection) {
		AvagadroBoardCell nextCell = getMachine().getBoard().getCurrentCell().getNeighbor(theDirection);
		if(nextCell!=null){
			getMachine().getBoard().setCurrentCell(nextCell);
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		getMachine().getAsciiTextGrid().WriteText(0, 0, "          ", AsciiColors.BLACK, AsciiColors.TRANSPARENT);
		getMachine().getAsciiTextGrid().WriteText(0, 0, String.format("%d",getMachine().getBoard().getScore()), AsciiColors.BLACK, AsciiColors.TRANSPARENT);
		if(getMachine().getBoard().isGameOver()){
			getMachine().getAsciiTextGrid().WriteText(0, 1, "Game Over", AsciiColors.RED, AsciiColors.TRANSPARENT);
		}else{
			getMachine().getAsciiTextGrid().WriteText(0, 1, "         ", AsciiColors.RED, AsciiColors.TRANSPARENT);
		}
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		getMachine().getSpriteBatch().setProjectionMatrix(getMachine().getCamera().combined);
		getMachine().getSpriteBatch().begin();
		getMachine().getBoard().draw(getMachine().getSpriteBatch());
		getMachine().getAsciiTextGrid().draw(getMachine().getSpriteBatch());
		getMachine().getSpriteBatch().end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start() {
		getMachine().getAsciiTextGrid().fill(AsciiCharacters.CHARACTER_0,AsciiColors.TRANSPARENT,AsciiColors.TRANSPARENT);		
		getMachine().getAsciiTextGrid().WriteText(0, getMachine().getAsciiTextGrid().getHeight()-1, "Play", AsciiColors.BLACK, AsciiColors.TRANSPARENT);
	}

}
