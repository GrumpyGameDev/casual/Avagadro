package com.playdeezgames.Avagadro.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.utils.TimeUtils;
import com.playdeezgames.Avagadro.AvagadroStateMachine;
import com.playdeezgames.common.Ascii.AsciiCharacters;
import com.playdeezgames.common.Ascii.AsciiColors;
import com.playdeezgames.common.Controller.ControllerAxis;
import com.playdeezgames.common.Controller.ControllerButton;
import com.playdeezgames.common.Controller.ControllerButtonState;
import com.playdeezgames.common.Controller.ControllerDPadState;
import com.playdeezgames.common.StateMachine.State;

public class GoodByeState extends BaseState implements State{

	private static final long GOODBYE_TIMER = 2000;
	private long startTime;

	public GoodByeState(AvagadroStateMachine theMachine) {
		super(theMachine);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onAxisChange(int controller, ControllerAxis axis, float value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onButtonChange(int controller, ControllerButton button,
			ControllerButtonState state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDPadChange(int controller, ControllerDPadState state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		getMachine().getSpriteBatch().setProjectionMatrix(getMachine().getCamera().combined);
		getMachine().getSpriteBatch().begin();
		getMachine().getAsciiTextGrid().draw(getMachine().getSpriteBatch());
		getMachine().getSpriteBatch().end();
		if(TimeUtils.millis()-startTime>GOODBYE_TIMER){
			Gdx.app.exit();
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start() {
		getMachine().getAsciiTextGrid().fill(AsciiCharacters.CHARACTER_0,AsciiColors.TRANSPARENT,AsciiColors.TRANSPARENT);		
		getMachine().getAsciiTextGrid().WriteText(0, getMachine().getAsciiTextGrid().getHeight()-1, "Thanks for playing!", AsciiColors.BLACK, AsciiColors.TRANSPARENT);
		startTime=TimeUtils.millis();
	}

}
