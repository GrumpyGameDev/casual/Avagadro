package com.playdeezgames.Avagadro.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.playdeezgames.Avagadro.AvagadroStateMachine;
import com.playdeezgames.Avagadro.Enums.AvagadroStates;
import com.playdeezgames.common.Ascii.AsciiColors;
import com.playdeezgames.common.Ascii.AsciiMenu;
import com.playdeezgames.common.Ascii.AsciiMenuItem;
import com.playdeezgames.common.Ascii.AsciiCharacters;
import com.playdeezgames.common.Controller.ControllerAxis;
import com.playdeezgames.common.Controller.ControllerButton;
import com.playdeezgames.common.Controller.ControllerButtonState;
import com.playdeezgames.common.Controller.ControllerDPadState;
import com.playdeezgames.common.StateMachine.State;

public class MainMenuState extends BaseState implements State{

	private static final int HILITE_ABOUT = 5;
	private static final int HILITE_HOW_TO_PLAY = 3;
	private static final int HILITE_QUIT = 6;
	private static final int HILITE_START = 0;
	private static final int HILITE_RESUME = 1;
	private static final int HILITE_ABANDON = 2;
	private static final int HILITE_OPTIONS = 4;
	private AsciiMenu asciiMenu;

	public MainMenuState(AvagadroStateMachine theMachine) {
		super(theMachine);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render() {
		asciiMenu.render(getMachine().getAsciiTextGrid(), 0, getMachine().getAsciiTextGrid().getHeight()-2);
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		getMachine().getSpriteBatch().setProjectionMatrix(getMachine().getCamera().combined);
		getMachine().getSpriteBatch().begin();
		getMachine().getAsciiTextGrid().draw(getMachine().getSpriteBatch());
		getMachine().getSpriteBatch().end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void create() {
		asciiMenu=new AsciiMenu();
		asciiMenu.getItems().add(new AsciiMenuItem("Start Game",true,true));
		asciiMenu.getItems().add(new AsciiMenuItem("Resume Game",true,false));
		asciiMenu.getItems().add(new AsciiMenuItem("Abandon Game",true,false));
		asciiMenu.getItems().add(new AsciiMenuItem("How to Play",true,true));
		asciiMenu.getItems().add(new AsciiMenuItem("Options",true,true));
		asciiMenu.getItems().add(new AsciiMenuItem("About",true,true));
		asciiMenu.getItems().add(new AsciiMenuItem("Exit",true,true));
		asciiMenu.getNormalCell().set(AsciiCharacters.CHARACTER_0,AsciiColors.BROWN,AsciiColors.TRANSPARENT);
		asciiMenu.getHiliteCell().set(AsciiCharacters.CHARACTER_0,AsciiColors.BROWN,AsciiColors.YELLOW);
		asciiMenu.getDisabledCell().set(AsciiCharacters.CHARACTER_0,AsciiColors.LIGHT_GRAY,AsciiColors.TRANSPARENT);
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start() {
		getMachine().getAsciiTextGrid().fill(AsciiCharacters.CHARACTER_0,AsciiColors.TRANSPARENT,AsciiColors.TRANSPARENT);
		getMachine().getAsciiTextGrid().WriteText(0, getMachine().getAsciiTextGrid().getHeight()-1, "Main Menu:", AsciiColors.BLACK, AsciiColors.TRANSPARENT);
		asciiMenu.getItems().get(HILITE_START).setEnabled(getMachine().canStartGame());
		asciiMenu.getItems().get(HILITE_RESUME).setEnabled(getMachine().canResumeGame());
		asciiMenu.getItems().get(HILITE_ABANDON).setEnabled(getMachine().canAbandonGame());
	}

	@Override
	public void onAxisChange(int controller, ControllerAxis axis, float value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onButtonChange(int controller, ControllerButton button,
			ControllerButtonState state) {
		if(state==ControllerButtonState.PRESSED){
			if(button==ControllerButton.GREEN){
				switch(asciiMenu.getHilite()){
				case HILITE_ABOUT:
					getMachine().setCurrent(AvagadroStates.ABOUT);
					break;
				case HILITE_HOW_TO_PLAY:
					getMachine().setCurrent(AvagadroStates.HOW_TO_PLAY);
					break;
				case HILITE_QUIT:
					getMachine().setCurrent(AvagadroStates.CONFIRM_QUIT);
					break;
				case HILITE_START:
					getMachine().startGame();
					getMachine().setCurrent(AvagadroStates.PLAY);
					break;
				case HILITE_RESUME:
					getMachine().setCurrent(AvagadroStates.PLAY);
					break;
				case HILITE_ABANDON:
					getMachine().setCurrent(AvagadroStates.CONFIRM_ABANDON);
					break;
				case HILITE_OPTIONS:
					getMachine().setCurrent(AvagadroStates.OPTIONS);
					break;
				default:
					break;
				}
			}
		}
	}

	@Override
	public void onDPadChange(int controller, ControllerDPadState state) {
		switch(state){
		case NORTH:
			asciiMenu.hilitePrevious();
			break;
		case SOUTH:
			asciiMenu.hiliteNext();
			break;
		default:
			break;
		}
	}

}
