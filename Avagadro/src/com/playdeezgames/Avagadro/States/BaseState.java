package com.playdeezgames.Avagadro.States;

import com.playdeezgames.Avagadro.AvagadroStateMachine;

public class BaseState{
	
	private AvagadroStateMachine machine = null;
	
	public AvagadroStateMachine getMachine(){
		return machine;
	}
	
	private void setMachine(AvagadroStateMachine theMachine){
		machine = theMachine;
	}
	
	public BaseState(AvagadroStateMachine theMachine){
		setMachine(theMachine);
	}
}
