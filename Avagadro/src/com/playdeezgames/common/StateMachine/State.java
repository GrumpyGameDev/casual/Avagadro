package com.playdeezgames.common.StateMachine;

import com.playdeezgames.common.Controller.AbstractControllerListener;

public interface State extends AbstractControllerListener{

	void dispose();

	void render();

	void resize(int width, int height);

	void pause();

	void resume();

	void create();

	void finish();

	void start();
}
