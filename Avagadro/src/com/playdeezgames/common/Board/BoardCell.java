package com.playdeezgames.common.Board;

import java.util.HashMap;
import java.util.Map;

public class BoardCell<D extends DirectionDescriptor<D>,T extends BoardCell<D,T>> {
	private Map<D,T> neighbors = new HashMap<D,T>();
	public T getNeighbor(D theDirection){
		return neighbors.get(theDirection);
	}
	public void setNeighbor(D theDirection,T theNeighbor){
		neighbors.put(theDirection, theNeighbor);
	}
}
