package com.playdeezgames.common.Board;

import java.util.ArrayList;
import java.util.List;

public class Board<D extends DirectionDescriptor<D>,T extends BoardCell<D,T>> {
	private List<BoardColumn<D,T>> columns = new ArrayList<BoardColumn<D,T>>();
	public int size(){
		return columns.size();
	}
	public BoardColumn<D,T> get(int index){
		if(index>=0 && index<size()){
			return columns.get(index);
		}else{
			return null;
		}
	}
	public T getCell(int column,int row){
		BoardColumn<D,T> boardColumn=get(column);
		if(boardColumn!=null){
			return boardColumn.get(row);
		}else{
			return null;
		}
	}
	public Board(int columns,int rows,BoardCellFactory<D,T> factory,D[] neighborDirections){
		while(columns>size()){
			this.columns.add(new BoardColumn<D,T>(rows,factory));
		}
		for(int column=0;column<columns;++column){
			for(int row=0;row<rows;++row){
				T cell = getCell(column,row);
				for(D direction:neighborDirections){
					int nextColumn = direction.nextColumn(column, row);
					int nextRow=direction.nextRow(column, row);
					T nextCell = getCell(nextColumn,nextRow);
					if(nextCell!=null){
						cell.setNeighbor(direction, nextCell);
					}
				}
			}
		}
	}
}
