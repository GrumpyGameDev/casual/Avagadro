package com.playdeezgames.common.Board;

import java.util.ArrayList;
import java.util.List;

public class BoardColumn<D extends DirectionDescriptor<D>,T extends BoardCell<D,T>> {
	private List<T> cells = new ArrayList<T>();
	public int size(){
		return cells.size();
	}
	public T get(int index){
		if(index>=0 && index<size()){
			return cells.get(index);
		}else{
			return null;
		}
	}
	public BoardColumn(int cellCount,BoardCellFactory<D,T> factory){
		while(cells.size()<cellCount){
			cells.add(factory.create());
		}
	}
}
