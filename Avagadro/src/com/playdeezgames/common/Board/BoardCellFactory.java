package com.playdeezgames.common.Board;

public interface BoardCellFactory<D extends DirectionDescriptor<D>,T extends BoardCell<D,T>> {
	T create();
}
