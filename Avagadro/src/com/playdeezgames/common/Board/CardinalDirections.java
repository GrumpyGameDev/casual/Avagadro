package com.playdeezgames.common.Board;

public enum CardinalDirections implements DirectionDescriptor<CardinalDirections>{
	NORTH(0,1),
	EAST(1,0),
	SOUTH(0,-1),
	WEST(-1,0);
	int deltaColumn;
	int deltaRow;
	
	@Override
	public CardinalDirections next() {
		switch(this){
		case NORTH:
			return CardinalDirections.EAST;
		case EAST:
			return CardinalDirections.SOUTH;
		case SOUTH:
			return CardinalDirections.WEST;
		case WEST:
			return CardinalDirections.NORTH;
		default:
			return null;
		}
	}

	@Override
	public CardinalDirections previous() {
		switch(this){
		case NORTH:
			return CardinalDirections.WEST;
		case EAST:
			return CardinalDirections.NORTH;
		case SOUTH:
			return CardinalDirections.EAST;
		case WEST:
			return CardinalDirections.SOUTH;
		default:
			return null;
		}
	}

	@Override
	public CardinalDirections opposite() {
		switch(this){
		case NORTH:
			return CardinalDirections.SOUTH;
		case EAST:
			return CardinalDirections.WEST;
		case SOUTH:
			return CardinalDirections.NORTH;
		case WEST:
			return CardinalDirections.EAST;
		default:
			return null;
		}
	}

	@Override
	public int nextColumn(int column, int row, int steps) {
		return column+deltaColumn*steps;
	}

	@Override
	public int nextRow(int column, int row, int steps) {
		return row+deltaRow*steps;
	}

	@Override
	public int nextColumn(int column, int row) {
		return nextColumn(column,row,1);
	}

	@Override
	public int nextRow(int column, int row) {
		return nextRow(column,row,1);
	}

	CardinalDirections(int deltaColumn,int deltaRow){
		this.deltaColumn=deltaColumn;
		this.deltaRow=deltaRow;
	}
}
