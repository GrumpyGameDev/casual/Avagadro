package com.playdeezgames.common.Board;

public interface DirectionDescriptor<D> {
	D next();
	D previous();
	D opposite();
	int nextColumn(int column,int row,int steps);
	int nextRow(int column,int row,int steps);
	int nextColumn(int column,int row);
	int nextRow(int column,int row);
}
