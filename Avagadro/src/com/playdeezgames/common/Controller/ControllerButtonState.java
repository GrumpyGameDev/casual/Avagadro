package com.playdeezgames.common.Controller;

public enum ControllerButtonState {
	PRESSED,
	RELEASED;
}
