package com.playdeezgames.common.Controller;

public enum ControllerAxis {
	LEFT_STICK_X,
	LEFT_STICK_Y,
	RIGHT_STICK_X,
	RIGHT_STICK_Y,
	LEFT_TRIGGER,
	RIGHT_TRIGGER;
}
