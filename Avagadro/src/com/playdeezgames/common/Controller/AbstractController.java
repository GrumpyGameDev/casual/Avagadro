package com.playdeezgames.common.Controller;

public interface AbstractController {
	void addListener(AbstractControllerListener listener);
	void removeListener(AbstractControllerListener listener);
}
