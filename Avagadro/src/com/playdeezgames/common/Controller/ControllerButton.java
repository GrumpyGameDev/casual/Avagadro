package com.playdeezgames.common.Controller;

public enum ControllerButton {
	GREEN,
	BLUE,
	YELLOW,
	RED,
	LEFT_SHOULDER,
	RIGHT_SHOULDER,
	LEFT_STICK,
	RIGHT_STICK,
	PAUSE;
}
