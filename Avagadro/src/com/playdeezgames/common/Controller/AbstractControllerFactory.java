package com.playdeezgames.common.Controller;

import com.badlogic.gdx.controllers.Controller;

public interface AbstractControllerFactory {
	AbstractController createAbstractController(int controller,Controller theController);
}
