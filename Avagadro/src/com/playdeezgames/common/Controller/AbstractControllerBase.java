package com.playdeezgames.common.Controller;

import java.util.ArrayList;
import java.util.List;

public class AbstractControllerBase implements AbstractController{
	
	private int id;
	
	public AbstractControllerBase(int theId){
		id = theId;
	}
	
	public int getId(){
		return id;
	}
	
	protected List<AbstractControllerListener> listeners = new ArrayList<AbstractControllerListener>();
	
	@Override
	public void addListener(AbstractControllerListener listener) {
		removeListener(listener);
		listeners.add(listener);
	}

	@Override
	public void removeListener(AbstractControllerListener listener) {
		listeners.remove(listener);
	}

}
