package com.playdeezgames.common.Controller;


public interface AbstractControllerListener {
	void onAxisChange(int controller,ControllerAxis axis,float value);
	void onButtonChange(int controller,ControllerButton button, ControllerButtonState state);
	void onDPadChange(int controller,ControllerDPadState state);
}
