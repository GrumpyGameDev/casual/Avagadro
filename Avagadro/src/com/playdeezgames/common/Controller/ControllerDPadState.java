package com.playdeezgames.common.Controller;

public enum ControllerDPadState {
	NORTH,
	NORTHEAST,
	EAST,
	SOUTHEAST,
	SOUTH,
	SOUTHWEST,
	WEST,
	NORTHWEST,
	CENTER;
}
