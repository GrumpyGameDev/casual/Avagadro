package com.playdeezgames.common.Ascii;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.playdeezgames.common.Managers.TextureRegionManager;

public class AsciiTextCell extends AsciiTextCellData{
	private TextureRegionManager<AsciiTextures,AsciiCharacters> regionManager;
	private Sprite backgroundSprite;
	private Sprite foregroundSprite;
	@Override
	public void setBackgroundColor(AsciiColors theBackgroundColor){
		super.setBackgroundColor(theBackgroundColor);
		backgroundSprite.setColor(theBackgroundColor.getRed(),theBackgroundColor.getGreen(),theBackgroundColor.getBlue(),theBackgroundColor.getAlpha());
	}
	@Override
	public void setForegroundColor(AsciiColors theForegroundColor){
		super.setForegroundColor(theForegroundColor);
		foregroundSprite.setColor(theForegroundColor.getRed(),theForegroundColor.getGreen(),theForegroundColor.getBlue(),theForegroundColor.getAlpha());
	}
	@Override
	public void setCharacter(AsciiCharacters theCharacter){
		super.setCharacter(theCharacter);
		foregroundSprite.setRegion(regionManager.getTextureRegion(getCharacter()));
	}
	public AsciiTextCell(TextureRegionManager<AsciiTextures,AsciiCharacters> theRegionManager,float x,float y,float width,float height){
		regionManager=theRegionManager;
		backgroundSprite = new Sprite(regionManager.getTextureRegion(AsciiCharacters.CHARACTER_219));
		backgroundSprite.setSize(width, height);
		backgroundSprite.setPosition(x, y);
		backgroundSprite.setOrigin(0,0);
		foregroundSprite = new Sprite();
		foregroundSprite.setSize(width, height);
		foregroundSprite.setPosition(x, y);
		foregroundSprite.setOrigin(0,0);
		setBackgroundColor(getBackgroundColor());
		setForegroundColor(getForegroundColor());
		setCharacter(getCharacter());
	}
	public void draw(SpriteBatch batch){
		backgroundSprite.draw(batch);
		foregroundSprite.draw(batch);
	}
}
