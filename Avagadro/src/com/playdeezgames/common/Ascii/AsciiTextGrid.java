package com.playdeezgames.common.Ascii;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.playdeezgames.common.Managers.TextureRegionManager;

public class AsciiTextGrid {
	private List<AsciiTextRow> lines = new ArrayList<AsciiTextRow>();
	public AsciiTextRow getRow(int row){
		return lines.get(row);
	}
	public int getHeight(){
		return lines.size();
	}
	public int getWidth(){
		if(getHeight()==0){
			return 0;
		}else{
			return getRow(0).getWidth();
		}
	}
	public AsciiTextGrid(TextureRegionManager<AsciiTextures,AsciiCharacters> theRegionManager,
			float x,float y,float cellWidth,float cellHeight,int columns,int rows){
		while(lines.size()<rows){
			lines.add(new AsciiTextRow(theRegionManager,x,y,cellWidth,cellHeight,columns));
			y+=cellHeight;
		}
	}
	public void draw(SpriteBatch batch){
		for(AsciiTextRow line: lines){
			line.draw(batch);
		}
	}
	public void WriteText(int column,int row,String text,AsciiColors theForegroundColor,AsciiColors theBackgroundColor){
		if(row>=0 && row<lines.size()){
			getRow(row).WriteText(column, text, theForegroundColor, theBackgroundColor);
		}
	}
	public void fill(int column,int row,int width,int height,AsciiCharacters theCharacter,AsciiColors theForegroundColor,AsciiColors theBackgroundColor){
		while(height>0){
			if(row>=0 && row<lines.size()){
				getRow(row).fill(column,width,theCharacter, theForegroundColor, theBackgroundColor);
			}
			row++;
			height--;
		}
	}
	public void fill(int column,int row,int width,int height,AsciiTextCellData cellData){
		fill(column,row,width,height,cellData.getCharacter(),cellData.getForegroundColor(),cellData.getBackgroundColor());
	}
	public void fill(AsciiCharacters theCharacter,AsciiColors theForegroundColor,AsciiColors theBackgroundColor){
		fill(0,0,getWidth(),getHeight(),theCharacter,theForegroundColor,theBackgroundColor);
	}
	public void fill(AsciiTextCellData cellData){
		fill(cellData.getCharacter(),cellData.getForegroundColor(),cellData.getBackgroundColor());
	}

}
