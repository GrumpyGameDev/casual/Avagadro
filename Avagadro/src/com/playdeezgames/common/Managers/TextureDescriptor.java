package com.playdeezgames.common.Managers;

import com.badlogic.gdx.graphics.Texture.TextureFilter;

public interface TextureDescriptor {
	String getFileName();
	TextureFilter getMinFilter();
	TextureFilter getMagFilter();
}
