package com.playdeezgames.common.Managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class TextureManager<T extends TextureDescriptor> {
	private Map<T,Texture> table = new HashMap<T,Texture>();
	public Texture getTexture(T theId){
		if(table.containsKey(theId)){
			return table.get(theId);
		}else{
			Texture theTexture=new Texture(Gdx.files.internal(theId.getFileName()));
			theTexture.setFilter(theId.getMinFilter(),theId.getMagFilter());
			table.put(theId, theTexture);
			return theTexture;
		}
	}
	public void dispose() {
		for(Texture texture:table.values()){
			texture.dispose();
		}
	}
}
