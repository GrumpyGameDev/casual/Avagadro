package com.playdeezgames.common.Managers;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;;

public class SoundManager<T extends SoundDescriptor> {
	private float volume=1.0f;
	private Map<T,Sound> table = new HashMap<T,Sound>();
	public void play(T id){
		Sound theSound = null;
		if(!table.containsKey(id)){
			theSound = Gdx.audio.newSound(Gdx.files.internal(id.getFileName()));
			table.put(id, theSound);
		}
		theSound.play(getVolume());
	}
	public void dispose(){
		for(Sound theSound : table.values()){
			theSound.stop();
			theSound.dispose();
		}
	}
	public float getVolume() {
		return volume;
	}
	public void setVolume(float volume) {
		this.volume = volume;
	}
}
