package com.playdeezgames.common.Managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class MusicManager<T extends MusicDescriptor> {
	private float volume=1.0f;
	private Music current=null;
	private Music next=null;
	private Music.OnCompletionListener listener;
	public float getVolume() {
		return volume;
	}
	public void setVolume(float volume) {
		this.volume = volume;
		if(current!=null){
			current.setVolume(volume);
		}
	}
	public void play(T id){
		next = Gdx.audio.newMusic(Gdx.files.internal(id.getFileName()));
		next.setOnCompletionListener(listener);
		next.setVolume(getVolume());
		if(current!=null){
			current.stop();
			current.dispose();
			current=null;
		}
		current=next;
		next=null;
		current.play();
	}
	public MusicManager(Music.OnCompletionListener listener){
		this.listener=listener;
	}
}
