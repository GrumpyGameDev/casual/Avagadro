package com.playdeezgames.common.Managers;

public interface TextureRegionDescriptor<T extends TextureDescriptor> {
	T getTextureDescriptor();
	int getX();
	int getY();
	int getWidth();
	int getHeight();
}
