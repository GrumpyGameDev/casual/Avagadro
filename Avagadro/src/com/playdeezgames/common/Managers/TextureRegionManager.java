package com.playdeezgames.common.Managers;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureRegionManager<T extends TextureDescriptor,U extends TextureRegionDescriptor<T>> {
	private Map<U,TextureRegion> table = new HashMap<U,TextureRegion>();
	private TextureManager<T> textureManager;
	public Sprite getNewSprite(U theRegionDescriptor){
		Sprite theSprite = new Sprite(getTextureRegion(theRegionDescriptor));
		theSprite.setSize(theRegionDescriptor.getWidth(), theRegionDescriptor.getHeight());
		return theSprite;
	}
	public TextureRegion getTextureRegion(U theRegionDescriptor){
		if(table.containsKey(theRegionDescriptor)){
			return table.get(theRegionDescriptor);
		}else{
			TextureRegion region = new TextureRegion(textureManager.getTexture(theRegionDescriptor.getTextureDescriptor()), theRegionDescriptor.getX(), theRegionDescriptor.getY(), theRegionDescriptor.getWidth(), theRegionDescriptor.getHeight());
			table.put(theRegionDescriptor, region);
			return region;
		}
	}
	public TextureRegionManager(TextureManager<T> theTextureManager){
		textureManager = theTextureManager;
	}
}
