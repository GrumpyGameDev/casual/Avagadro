package com.playdeezgames.common.Managers;

public interface SoundDescriptor {
	String getFileName();
}
