package com.playdeezgames.common.Generators;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class StandardWeightedGenerator<T> implements WeightedGenerator<T>{

	private Map<T,Integer> weights = new HashMap<T,Integer>();
	private int total = 0;
	private Random random;
	
	@Override
	public Integer getWeight(T theValue) {
		if(weights.containsKey(theValue)){
			return weights.get(theValue);
		}else{
			return 0;
		}
	}

	@Override
	public void setWeight(T theValue, Integer theWeight) {
		setTotal(getTotal()-getWeight(theValue));
		if(theWeight<=0){
			weights.remove(theValue);
		}else{
			weights.put(theValue, theWeight);
			setTotal(getTotal()+getWeight(theValue));
		}
	}

	private void setTotal(int i) {
		total=i;
	}

	private Integer getTotal() {
		return total;
	}

	@Override
	public T generate() {
		if(getTotal()==0){
			return null;
		}else{
			int generated = getRandom().nextInt(getTotal());
			for(Entry<T,Integer> entry:getWeights().entrySet()){
				if(generated<entry.getValue()){
					return entry.getKey();
				}else{
					generated-=entry.getValue();
				}
			}
			return null;
		}
	}

	private Map<T,Integer> getWeights() {
		return weights;
	}

	private Random getRandom() {
		if(random==null){
			setRandom(new Random());
		}
		return random;
	}

	private void setRandom(Random random) {
		this.random = random;
	}
	
	public StandardWeightedGenerator(Random random){
		setRandom(random);
	}
}
