package com.playdeezgames.common.Generators;

public interface WeightedGenerator<T> {
	Integer getWeight(T theValue);
	void setWeight(T theValue,Integer theWeight);
	T generate();
}
